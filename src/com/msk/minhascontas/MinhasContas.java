package com.msk.minhascontas;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.msk.minhascontas.db.DBContas;
import com.msk.minhascontas.paginadores.PaginadorDiarioContas;
import com.msk.minhascontas.paginadores.PaginadorMensalContas;

public class MinhasContas extends Activity implements OnClickListener {

	// BUSCA PREFERENCIAS DO USUARIO
	SharedPreferences buscaPreferencias = null;
	private Boolean bloqueioApp = false;
	private Boolean atualizaPagamento = false;
	private Boolean resumoMensal = true;
	private String senhaUsuario, tipo, categoria;
	private String[] despesas, receitas;
	private EditText senha;
	private Button acesso;

	DBContas db = new DBContas(this);
	Calendar c = Calendar.getInstance();
	Intent inicio = new Intent();
	Resources r;
	Thread tempo;

	ActionBar actionBar;

	private int dia, mes, ano;

	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		buscaPreferencias = PreferenceManager.getDefaultSharedPreferences(this);
		bloqueioApp = buscaPreferencias.getBoolean("acesso", false);
		atualizaPagamento = buscaPreferencias.getBoolean("pagamento", false);
		resumoMensal = buscaPreferencias.getBoolean("resumo", true);
		senhaUsuario = buscaPreferencias.getString("senha", "");

		CriaCategorias();
		
		if (bloqueioApp == true) {

			setContentView(R.layout.tela_bloqueio);

			senha = (EditText) findViewById(R.id.etSenha);
			acesso = (Button) findViewById(R.id.bEntra);

			acesso.setOnClickListener(this);

		} else {

			if (resumoMensal == true) {
				inicio.setClass(this, PaginadorMensalContas.class);

			} else {
				inicio.setClass(this, PaginadorDiarioContas.class);
			}

			startActivity(inicio);

			finish();
		}

	}

	private void CriaCategorias() {

		r = getResources();
		despesas = r.getStringArray(R.array.TipoDespesa);
		receitas = r.getStringArray(R.array.TipoReceita);

		db.open();
		int u = db.quantasCategorias();
		db.confirmaPagamentos();
		db.ajustaRepeticoesContas();
		db.close();

		if (u == 0) {

			// CRIA CATEGORIAS QUANDO NAO EXISTEM
			db.open();
			tipo = r.getString(R.string.linha_despesa);
			categoria = despesas[0];
			db.criaCategoriaConta(categoria, tipo, "paga", "repete_sempre",
					"nao_mostra");
			db.alteraCategoriaContas("Fixa", categoria, tipo);
			categoria = despesas[1];
			db.criaCategoriaConta(categoria, tipo, "paga", "repete",
					"nao_mostra");
			db.alteraCategoriaContas("Variavel", categoria, tipo);
			categoria = despesas[2];
			db.criaCategoriaConta(categoria, tipo, "paga", "repete", "mostra");
			db.alteraCategoriaContas("Prestacao", categoria, tipo);
			tipo = r.getString(R.string.linha_receita);
			categoria = receitas[3];
			db.criaCategoriaConta(categoria, tipo, "nao_paga", "repete",
					"nao_mostra");
			db.alteraCategoriaContas("Simples", categoria, tipo);
			tipo = r.getString(R.string.linha_aplicacoes);
			categoria = receitas[0];
			db.alteraCategoriaContas("Fundo", categoria, tipo);
			db.criaCategoriaConta(categoria, tipo, "paga", "repete",
					"nao_mostra");
			categoria = receitas[1];
			db.criaCategoriaConta(categoria, tipo, "paga", "repete",
					"nao_mostra");
			db.alteraCategoriaContas("Poupanca", categoria, tipo);
			categoria = receitas[2];
			db.criaCategoriaConta(categoria, tipo, "paga", "repete",
					"nao_mostra");
			db.alteraCategoriaContas("Previdencia", categoria, tipo);
			db.close();

		}

		if (atualizaPagamento == true) {

			dia = c.get(5);
			mes = c.get(2);
			ano = c.get(1);
			dia = dia + 1;

			db.open();
			db.atualizaPagamentoContas(dia, mes, ano);
			db.close();
		}

	}

	@Override
	public void onClick(View v) {

		if (senhaUsuario.equals(senha.getText().toString())) {

			if (resumoMensal == true) {
				inicio.setClass(this, PaginadorMensalContas.class);

			} else {
				inicio.setClass(this, PaginadorDiarioContas.class);
			}

			finish();
			startActivity(inicio);

		} else {

			Dialogo();

		}

	}

	private void Dialogo() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				new ContextThemeWrapper(this, R.style.TemaDialogo));

		builder.setTitle(R.string.titulo_senha);
		builder.setMessage(R.string.senha_errada);
		builder.setNeutralButton("OK", null);

		// create alert dialog
		AlertDialog alertDialog = builder.create();
		// show it
		alertDialog.show();
	}

}
