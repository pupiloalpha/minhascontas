package com.msk.minhascontas.db;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.msk.minhascontas.R;
import com.msk.minhascontas.info.BarraProgresso;

public class NovaConta extends ActionBarActivity implements
		RadioGroup.OnCheckedChangeListener, View.OnClickListener,
		AdapterView.OnItemSelectedListener {

	private static final int DATE_DIALOG_ID = 0;

	// ELEMENTOS DA TELA
	private ImageButton criaNovaConta;
	private LinearLayout juros;
	private TextView dataConta;
	private AutoCompleteTextView nomeConta;
	private EditText repeteConta, valorConta, jurosConta;
	private RadioGroup tipo;
	private CheckBox parcelarConta, pagamento;
	private Spinner classificaConta, intervaloRepete;

	// VARIAVEIS UTILIZADAS
	private static int dia, mes, ano;
	private int diaRepete, mesRepete, anoRepete;
	private int nRepete, qtRepete, intervalo;
	private String contaClasse, contaData, contaNome, contaPaga, contaTipo;
	private double contaValor, valorPrestacao, valorJuros;

	private Resources r;

	private Cursor despesas, naoDespesas;
	private SimpleCursorAdapter adapter;

	private final Calendar c = Calendar.getInstance();

	DBContas dbNovasContas = new DBContas(this);

	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cria_conta);
		setTheme(android.R.style.Theme);
		r = getResources();
		iniciar();
		usarActionBar();
		DataDeHoje(dia, mes, ano);
		MostraClasseConta();
		tipo.setOnCheckedChangeListener(this);
		parcelarConta.setVisibility(View.INVISIBLE);
		dataConta.setOnClickListener(this);
		criaNovaConta.setOnClickListener(this);
		pagamento.setOnClickListener(this);
		classificaConta.setOnItemSelectedListener(this);
		intervaloRepete.setOnItemSelectedListener(this);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void iniciar() {
		nomeConta = ((AutoCompleteTextView) findViewById(R.id.acNomeNovaConta));
		valorConta = ((EditText) findViewById(R.id.etValorNovaConta));
		jurosConta = (EditText) findViewById(R.id.etJurosNovaConta);
		repeteConta = ((EditText) findViewById(R.id.etRepeticoes));
		criaNovaConta = ((ImageButton) findViewById(R.id.ibNovaConta));
		tipo = ((RadioGroup) findViewById(R.id.rgTipoNovaConta));
		parcelarConta = ((CheckBox) findViewById(R.id.cbValorParcelar));
		pagamento = ((CheckBox) findViewById(R.id.cbPagamento));
		dataConta = ((TextView) findViewById(R.id.tvDataNovaConta));
		juros = (LinearLayout) findViewById(R.id.juros_aplicados);
		classificaConta = ((Spinner) findViewById(R.id.spClasseConta));
		intervaloRepete = ((Spinner) findViewById(R.id.spRepeticoes));
		intervaloRepete.setSelection(2);
		ArrayAdapter completa = new ArrayAdapter(this,
				android.R.layout.simple_dropdown_item_1line, getResources()
						.getStringArray(R.array.NomeConta));
		nomeConta.setAdapter(completa);
		dia = c.get(Calendar.DAY_OF_MONTH);
		mes = c.get(Calendar.MONTH);
		ano = c.get(Calendar.YEAR);
		contaTipo = r.getString(R.string.linha_despesa);
		contaClasse = r.getString(R.string.linha_despVar);
		contaPaga = "falta";
		intervalo = 300;

	}

	private void DataDeHoje(int dia, int mes, int ano) {

		dataConta.setText(dia + "/" + (mes + 1) + "/" + ano);
		diaRepete = dia;
		anoRepete = ano;
	}

	private void MostraClasseConta() {
		int i = 0;
		dbNovasContas.open();
		despesas = dbNovasContas.buscaCategoriaPorTipo(r
				.getString(R.string.linha_despesa));
		naoDespesas = dbNovasContas.buscaCategoriaSemTipo(r
				.getString(R.string.linha_despesa));
		if (contaTipo.equals(r.getString(R.string.linha_receita))) {
			adapter = new SimpleCursorAdapter(this,
					android.R.layout.simple_spinner_item, naoDespesas,
					new String[] { "categoria" },
					new int[] { android.R.id.text1 }, 0);
			i = naoDespesas.getCount() - 1;
		} else {
			adapter = new SimpleCursorAdapter(this,
					android.R.layout.simple_spinner_item, despesas,
					new String[] { "categoria" },
					new int[] { android.R.id.text1 }, 0);
			i = 1;
		}
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		classificaConta.setAdapter(adapter);
		classificaConta.setSelection(i);
		dbNovasContas.close();
	}

	@Override
	public void onCheckedChanged(RadioGroup tipos, int tipoId) {

		switch (tipoId) {

		case R.id.rDespNovaConta:
			contaTipo = r.getString(R.string.linha_despesa);
			pagamento.setVisibility(View.VISIBLE);
			parcelarConta.setVisibility(View.INVISIBLE);
			break;
		case R.id.rRecNovaConta:
			contaTipo = r.getString(R.string.linha_receita);
			pagamento.setVisibility(View.INVISIBLE);
			parcelarConta.setVisibility(View.INVISIBLE);
			break;
		}

		MostraClasseConta();

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View paramView) {

		switch (paramView.getId()) {

		case R.id.tvDataNovaConta:
			showDialog(DATE_DIALOG_ID);
			break;
		case R.id.cbPagamento:
			if (pagamento.isChecked()) {
				contaPaga = "paguei";
			} else {
				contaPaga = "falta";
			}
			break;
		case R.id.ibNovaConta:

			if (nomeConta.getText().toString().equals(""))
				contaNome = r.getString(R.string.sem_nome);
			else {
				contaNome = nomeConta.getText().toString();
			}
			String nomeConta1 = contaNome;
			String nomeConta2 = contaNome;
			dbNovasContas.open();
			int a = dbNovasContas.quantasContasPorNomeNoDia(nomeConta1, dia,
					mes, ano);
			int b = 1;

			if (a != 0) {
				while (a != 0) {
					nomeConta2 = nomeConta1 + b;
					a = dbNovasContas.quantasContasPorNomeNoDia(nomeConta2,
							dia, mes, ano);
					b = b + 1;
				}
				contaNome = nomeConta2;
			}

			if (!repeteConta.getText().toString().equals(""))
				qtRepete = Integer.parseInt(repeteConta.getText().toString());
			else
				qtRepete = 1;

			if (!jurosConta.getText().toString().equals("")) {
				valorJuros = Double
						.parseDouble(jurosConta.getText().toString());
				valorJuros = valorJuros / 100;
			} else {
				valorJuros = 0.0D;
			}

			if (!valorConta.getText().toString().equals("")) {

				if (parcelarConta.isChecked()) {
					valorPrestacao = Double.parseDouble(valorConta.getText()
							.toString());
					valorPrestacao = valorPrestacao / qtRepete;
					contaValor = valorPrestacao;

					String[] prestacao = r.getStringArray(R.array.TipoDespesa);

					if (contaClasse.equals(prestacao[2]) && qtRepete > 1
							&& valorJuros != 0)
						contaValor = contaValor
								* ((valorJuros) / (1.0D - (1 / (Math.pow(
										(valorJuros + 1.0D), qtRepete)))));
				} else {
					contaValor = Double
							.valueOf(valorConta.getText().toString());
				}

				contaData = dataConta.getText().toString();

				diaRepete = dia;
				mesRepete = mes;
				anoRepete = ano;

				if (repeteConta.getText().toString().equals("")
						|| repeteConta.getText().toString().equals("0")) {
					dbNovasContas.geraConta(contaNome, contaTipo, contaClasse,
							contaPaga, contaData, dia, mes, ano, contaValor,
							qtRepete, 1, intervalo);

					if (contaTipo
							.equals(r.getString(R.string.linha_aplicacoes))) {
						// USUARIO ESCOLHE TRANSFORMAR APLICACAO EM DESPESA
						new AlertDialog.Builder(new ContextThemeWrapper(this,
								R.style.TemaDialogo))
								.setTitle(R.string.titulo_despesa_saque)
								.setMessage(R.string.texto_despesa_saque)
								.setPositiveButton(R.string.ok,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface pDialogo,
													int pInt) {
												String[] despesas = r
														.getStringArray(R.array.TipoDespesa);
												dbNovasContas.geraConta(
														contaNome,
														r.getString(R.string.linha_despesa),
														despesas[1], contaPaga,
														contaData, dia, mes,
														ano, contaValor,
														qtRepete, 1, intervalo);
											}
										})
								.setNegativeButton(R.string.cancelar,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface pDialogo,
													int pInt) {
												pDialogo.dismiss();
											}
										}).show();
					}

				} else {

					new BarraProgresso(this, getResources().getString(
							R.string.dica_titulo_barra), getResources()
							.getString(R.string.dica_barra_progresso),
							qtRepete, 0).execute();

					// Metodo para repetir conta
					dbNovasContas.geraConta(contaNome, contaTipo, contaClasse,
							contaPaga, contaData, dia, mes, ano, contaValor,
							qtRepete, 1, intervalo);

					for (int i = 1; i < qtRepete; i++) {

						nRepete = i + 1;

						if (intervalo == 300) { // Repeticao mensal

							mesRepete = (1 + mesRepete);

							if (mesRepete > 11) {
								mesRepete = 0;
								anoRepete = (1 + anoRepete);
							}

						} else if (intervalo == 3650) { // Repeticao anual
							anoRepete = anoRepete + 1;
						} else { // Repeticao diaria ou semanal
							diaRepete = diaRepete + (intervalo - 100);
							// Fevereiro ano normal
							if (diaRepete > 28
									&& mesRepete == 1
									&& (int) Math
											.IEEEremainder(anoRepete, 4.0D) != 0) {
								diaRepete = diaRepete - 28;
								mesRepete = 2;
							} else if (diaRepete > 29
									&& mesRepete == 1
									&& (int) Math
											.IEEEremainder(anoRepete, 4.0D) == 0) {
								diaRepete = diaRepete - 29;
								mesRepete = 2;
							} else if (diaRepete > 30)
								if (mesRepete == 3 || mesRepete == 5
										|| mesRepete == 6 || mesRepete == 8
										|| mesRepete == 10) {
									diaRepete = diaRepete - 30;
									mesRepete = mesRepete + 1;

								} else if (diaRepete > 31)
									if (mesRepete == 0 || mesRepete == 2
											|| mesRepete == 4 || mesRepete == 7
											|| mesRepete == 9
											|| mesRepete == 11) {
										diaRepete = diaRepete - 31;
										mesRepete = mesRepete + 1;
										if (mesRepete > 11) {
											mesRepete = 0;
											anoRepete = (1 + anoRepete);
										}
									}
						}

						int k = 1 + mesRepete;
						String str = diaRepete + "/" + k + "/" + anoRepete;

						// Ajustes para acrescentar rendimento mensal/anual em
						// aplicacao
						String[] aplicacao = r
								.getStringArray(R.array.TipoReceita);

						if (contaClasse.equals(aplicacao[0])
								|| contaClasse.equals(aplicacao[2]))
							contaValor = contaValor * (1.0D + valorJuros);

						dbNovasContas.geraConta(contaNome, contaTipo,
								contaClasse, contaPaga, str, diaRepete,
								mesRepete, anoRepete, contaValor, qtRepete,
								nRepete, intervalo);
					}

				}
			}
			dbNovasContas.close();
			Toast.makeText(
					getApplicationContext(),
					String.format(
							getResources()
									.getString(R.string.dica_conta_criada),
							contaNome), Toast.LENGTH_SHORT).show();
			nomeConta.setText("");
			repeteConta.setText("");
			valorConta.setText("");
			jurosConta.setText("");
			setResult(RESULT_OK, null);
			finish();
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> spinner, View itemsp,
			int posicao, long paramLong) {

		if (spinner.getId() == R.id.spClasseConta) {
			if (contaTipo.equals(r.getString(R.string.linha_despesa))) {

				dbNovasContas.open();
				despesas.moveToPosition(posicao);
				contaClasse = despesas.getString(1);
				contaTipo = despesas.getString(2);
				dbNovasContas.close();
				juros.setVisibility(View.VISIBLE);
				pagamento.setText(R.string.dica_pagamento);
				pagamento.setVisibility(View.VISIBLE);
				if (posicao == 2)
					parcelarConta.setVisibility(View.VISIBLE);
				else
					parcelarConta.setVisibility(View.INVISIBLE);

				if (posicao == 0)
					repeteConta.setText(120 + "");
				qtRepete = 120;

			} else {

				dbNovasContas.open();
				naoDespesas.moveToPosition(posicao);
				contaClasse = naoDespesas.getString(1);
				contaTipo = naoDespesas.getString(2);
				dbNovasContas.close();
				juros.setVisibility(View.VISIBLE);
				parcelarConta.setVisibility(View.INVISIBLE);
			}

			repeteConta.setText("");
			parcelarConta.setChecked(false);

		}

		if (spinner.getId() == R.id.spRepeticoes) {

			switch (posicao) {

			case 0: // Diariamente
				intervalo = 101;
				break;
			case 1: // Semanalmente
				intervalo = 107;
				break;
			case 2: // Mensalmente
				intervalo = 300;
				break;
			case 3: // Anualmente
				intervalo = 3650;
				break;
			}

		}

	}

	// modifica as informacoes da data apos modificacao
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			ano = year;
			mes = monthOfYear;
			dia = dayOfMonth;
			DataDeHoje(dia, mes, ano);
		}
	};

	// Atualiza a data escolhida pelo usuario
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(new ContextThemeWrapper(this,
					R.style.TemaDialogo), mDateSetListener, ano, mes, dia);
		}
		return null;
	}

	@Override
	public void onNothingSelected(AdapterView<?> paramAdapterView) {
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

	}

	@Override
	protected void onDestroy() {
		setResult(RESULT_OK, null);
		super.onDestroy();
	}

}
