package com.msk.minhascontas.db;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.msk.minhascontas.R;
import com.msk.minhascontas.info.BarraProgresso;

@SuppressLint("NewApi")
public class EditaConta extends ActionBarActivity implements
		View.OnClickListener, RadioGroup.OnCheckedChangeListener,
		OnItemSelectedListener {
	static final int DIALOGO_DATA_ID = 0;

	// ELEMENTOS DA TELA
	private AutoCompleteTextView nome;
	private EditText valor, prestacoes;
	private ImageButton modifica;
	private RadioGroup tipo;
	private RadioButton rec, desp;
	private CheckBox pagamento;
	private Spinner classificaConta, intervaloRepete;
	private TextView data;

	// VARIAVEIS UTILIZADAS
	private double valorConta, valorNovoConta;
	private long idConta;
	private int ano, anoPrest, dia, diaVenc, mes, mesPrest, nPrest, qtPrest,
			intervalo, qtConta, nr;
	private int[] dmaConta, repeteConta;
	private String classeConta, dataConta, nomeConta, tipoConta, pagouConta,
			novoPagouConta, novoNomeConta;
	private String[] dadosConta;

	private Resources r;

	private Cursor despesas, naoDespesas;
	private SimpleCursorAdapter adapter;

	DBContas dbContaParaEditar = new DBContas(this);

	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.edita_conta);
		setTheme(android.R.style.Theme);
		Bundle localBundle = getIntent().getExtras();
		idConta = localBundle.getLong("id");
		r = getResources();
		Iniciar();
		usarActionBar();
		PegaConta();
		MostraDados();

		classificaConta.setOnItemSelectedListener(this);
		intervaloRepete.setOnItemSelectedListener(this);
		pagamento.setOnClickListener(this);
		data.setOnClickListener(this);
		modifica.setOnClickListener(this);
		tipo.setOnCheckedChangeListener(this);

	}

	private void Iniciar() {
		nome = (AutoCompleteTextView) findViewById(R.id.acNomeContaModificada);
		pagamento = (CheckBox) findViewById(R.id.cbPagamento);
		data = (TextView) findViewById(R.id.tvDataContaModificada);
		valor = (EditText) findViewById(R.id.etValorNovo);
		tipo = (RadioGroup) findViewById(R.id.rgTipoContaModificada);
		rec = (RadioButton) findViewById(R.id.rRecContaModificada);
		desp = (RadioButton) findViewById(R.id.rDespContaModificada);
		prestacoes = (EditText) findViewById(R.id.etPrestacoes);
		modifica = (ImageButton) findViewById(R.id.ibModificaConta);
		classificaConta = (Spinner) findViewById(R.id.spClassificaConta);
		intervaloRepete = (Spinner) findViewById(R.id.spRepeticoes);
		@SuppressWarnings({ "unchecked", "rawtypes" })
		ArrayAdapter autocompleta = new ArrayAdapter(this,
				android.R.layout.simple_dropdown_item_1line, getResources()
						.getStringArray(R.array.NomeConta));
		nome.setAdapter(autocompleta);
		pagamento.setVisibility(View.GONE);
		dia = mes = ano = 0;

	}

	private void PegaConta() {
		dbContaParaEditar.open();
		dmaConta = dbContaParaEditar.mostraDMAConta(idConta);
		dia = dmaConta[0];
		mes = dmaConta[1];
		ano = dmaConta[2];
		dadosConta = dbContaParaEditar.mostraDadosConta(idConta);
		nomeConta = dadosConta[0];
		tipoConta = dadosConta[1];
		classeConta = dadosConta[2];
		pagouConta = dadosConta[3];
		novoPagouConta = pagouConta;
		dataConta = dadosConta[4];
		valorConta = dbContaParaEditar.mostraValorConta(idConta);
		repeteConta = dbContaParaEditar.mostraRepeticaoConta(idConta);
		qtPrest = repeteConta[0];
		nPrest = repeteConta[1];
		intervalo = repeteConta[2];
		dbContaParaEditar.close();
	}

	private void MostraDados() {
		nome.setText(nomeConta);
		dataConta = (dia + "/" + (1 + mes) + "/" + ano);
		data.setText(dataConta);
		valor.setText(String.format("%.2f", valorConta).replace(",", "."));
		pagamento.setChecked(false);

		if (pagouConta.equals("paguei"))
			pagamento.setChecked(true);

		int i = 1;
		dbContaParaEditar.open();
		despesas = dbContaParaEditar.buscaCategoriaPorTipo(r
				.getString(R.string.linha_despesa));
		naoDespesas = dbContaParaEditar.buscaCategoriaSemTipo(r
				.getString(R.string.linha_despesa));
		if (tipoConta.equals(r.getString(R.string.linha_despesa))) {
			adapter = new SimpleCursorAdapter(this,
					android.R.layout.simple_spinner_item, despesas,
					new String[] { "categoria" },
					new int[] { android.R.id.text1 }, 0);

			despesas.moveToFirst();
			for (int j = 0; j < despesas.getCount(); j++) {
				if (dadosConta[2].equals(despesas.getString(1)))
					i = j;
				despesas.moveToNext();
			}

			rec.setChecked(false);
			desp.setChecked(true);
			pagamento.setText(R.string.dica_pagamento);
			pagamento.setVisibility(View.VISIBLE);

		} else {
			adapter = new SimpleCursorAdapter(this,
					android.R.layout.simple_spinner_item, naoDespesas,
					new String[] { "categoria" },
					new int[] { android.R.id.text1 }, 0);
			i = naoDespesas.getCount() - 1;
			naoDespesas.moveToFirst();
			for (int j = 0; j < naoDespesas.getCount(); j++) {
				if (dadosConta[2].equals(naoDespesas.getString(1)))
					i = j;
				naoDespesas.moveToNext();
			}

			rec.setChecked(true);
			desp.setChecked(false);
			pagamento.setVisibility(View.INVISIBLE);
		}

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		classificaConta.setAdapter(adapter);
		classificaConta.setSelection(i);

		dbContaParaEditar.close();

		prestacoes.setText(qtPrest + "");

		if (intervalo == 101)
			intervaloRepete.setSelection(0);
		else if (intervalo == 107)
			intervaloRepete.setSelection(1);
		else if (intervalo == 3650)
			intervaloRepete.setSelection(3);
		else if (intervalo == 300)
			intervaloRepete.setSelection(2);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View paramView) {

		switch (paramView.getId()) {
		case R.id.tvDataContaModificada:
			showDialog(DIALOGO_DATA_ID);
			break;
		case R.id.cbPagamento:
			if (pagamento.isChecked()) {
				novoPagouConta = "paguei";
			} else {
				novoPagouConta = "falta";
			}
			break;
		case R.id.ibModificaConta:

			qtConta = 0;
			dbContaParaEditar.open();
			qtConta = dbContaParaEditar.quantasContasPorNome(nomeConta);
			dbContaParaEditar.close();

			if (qtConta == 1) {
				nr = 0;
				ModificaDadosConta();
				setResult(RESULT_OK, null);
				finish();
			} else {
				Dialogo();
			}

			break;
		}

	}

	private void Dialogo() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				new ContextThemeWrapper(this, R.style.TemaDialogo));

		// set title
		alertDialogBuilder.setTitle(getString(R.string.dica_menu_edicao));

		// set dialog message
		alertDialogBuilder.setItems(R.array.TipoAjusteConta,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						switch (id) {
						case 0: // Edita uma conta
							nr = 0;
							break;
						case 1: // Edita uma conta e as repeticoes posteriores
							dbContaParaEditar.open();
							int[] repete = dbContaParaEditar
									.mostraRepeticaoConta(idConta);
							dbContaParaEditar.close();
							nr = repete[1];

							break;
						case 2: // Edita todas as repeticoes
							nr = 1;
							break;
						}

						ModificaDadosConta();
						setResult(RESULT_OK, null);
						finish();

					}

				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	private void ModificaDadosConta() {

		if (!nome.getText().toString().equals("")) {
			novoNomeConta = nome.getText().toString();
		} else
			novoNomeConta = r.getString(R.string.sem_nome);

		dbContaParaEditar.open();

		// Confere o nome digitado com o nome da conta
		if (!nomeConta.equals(novoNomeConta)) {

			String nomeConta1 = novoNomeConta;
			String nomeConta2 = novoNomeConta;

			int a = dbContaParaEditar.quantasContasPorNomeNoDia(nomeConta1,
					dia, mes, ano);
			int b = 1;

			if (a != 0) {
				while (a != 0) {
					nomeConta2 = nomeConta1 + b;
					a = dbContaParaEditar.quantasContasPorNomeNoDia(nomeConta2,
							dia, mes, ano);
					b = b + 1;
				}
				novoNomeConta = nomeConta2;
			}

		} else {
			novoNomeConta = nomeConta;
		}

		dbContaParaEditar.alteraNomeConta(idConta, novoNomeConta);

		if (!valor.getText().toString().equals(""))
			valorNovoConta = Double.parseDouble(valor.getText().toString());
		else
			valorNovoConta = 0;

		// Confere o valor digitado com o valor da conta
		if (valorConta != valorNovoConta || !pagouConta.equals(novoPagouConta)) {
			dbContaParaEditar.alteraValorConta(idConta, valorNovoConta,
					novoPagouConta);
		}

		// Atualiza tipo, classe e data da conta
		dbContaParaEditar.alteraTipoConta(idConta, tipoConta);
		dbContaParaEditar.alteraClasseConta(idConta, classeConta);

		diaVenc = dia;
		mesPrest = mes;
		anoPrest = ano;

		if (!prestacoes.getText().toString().equals(""))
			qtPrest = Integer.parseInt(prestacoes.getText().toString());
		else
			qtPrest = 0;

		if (dmaConta[0] != diaVenc || dmaConta[1] != mesPrest
				|| dmaConta[2] != anoPrest) {

			dbContaParaEditar.alteraDataConta(idConta, data.getText()
					.toString(), diaVenc, mesPrest, anoPrest);

		}

		if (qtPrest != repeteConta[0] || repeteConta[2] != intervalo || nr > 0) {

			new BarraProgresso(this, getResources().getString(
					R.string.dica_titulo_barra), getResources().getString(
					R.string.dica_barra_progresso), qtPrest, 0).execute();

			if (nr == 1 && nPrest != 1) {

				// Metodo para obter data da primeira conta da repeticao
				for (int i = nPrest; i > 0; i--) {

					nPrest = i;

					if (intervalo == 300) { // Repeticao mensal

						mesPrest = (1 - mesPrest);

						if (mesPrest < 0) {
							mesPrest = 11;
							anoPrest = (anoPrest - 1);
						}

					} else if (intervalo == 3650) { // Repeticao anual
						anoPrest = anoPrest - 1;
					} else { // Repeticao diaria ou semanal
						diaVenc = diaVenc - (intervalo - 100);
						// Fevereiro ano normal
						if (diaVenc < 0
								&& mesPrest == 2
								&& (int) Math.IEEEremainder(anoPrest, 4.0D) != 0) {
							diaVenc = diaVenc + 28;
							mesPrest = 1;
						} else if (diaVenc < 0
								&& mesPrest == 2
								&& (int) Math.IEEEremainder(anoPrest, 4.0D) == 0) {
							diaVenc = diaVenc + 29;
							mesPrest = 1;
						} else if (diaVenc < 0)
							if (mesPrest == 3 || mesPrest == 5 || mesPrest == 6
									|| mesPrest == 8 || mesPrest == 10) {
								diaVenc = diaVenc + 30;
								mesPrest = mesPrest - 1;

							} else if (mesPrest == 0 || mesPrest == 2
									|| mesPrest == 4 || mesPrest == 7
									|| mesPrest == 9 || mesPrest == 11) {
								diaVenc = diaVenc + 31;
								mesPrest = mesPrest - 1;
								if (mesPrest < 0) {
									mesPrest = 11;
									anoPrest = (anoPrest - 1);
								}
							}
					}
				}
			}

			dbContaParaEditar.excluiSerieContaPorNome(nomeConta, nPrest);

			// Metodo para repetir conta
			for (int i = nPrest; i <= qtPrest; i++) {

				nPrest = i;
				
				int k = 1 + mesPrest;
				String str = diaVenc + "/" + k + "/" + anoPrest;

				dbContaParaEditar.geraConta(novoNomeConta, tipoConta,
						classeConta, novoPagouConta, str, diaVenc, mesPrest,
						anoPrest, valorNovoConta, qtPrest, nPrest, intervalo);

				// CORRECAO DAS DATAS PARA OS INTERVALOS DE REPETICAO
				if (intervalo == 300) { // Repeticao mensal

					mesPrest = (1 + mesPrest);

					if (mesPrest > 11) {
						mesPrest = 0;
						anoPrest = (1 + anoPrest);
					}

				} else if (intervalo == 3650) { // Repeticao anual
					anoPrest = anoPrest + 1;
				} else { // Repeticao diaria ou semanal
					diaVenc = diaVenc + (intervalo - 100);
					// Fevereiro ano normal
					if (diaVenc > 28 && mesPrest == 1
							&& (int) Math.IEEEremainder(anoPrest, 4.0D) != 0) {
						diaVenc = diaVenc - 28;
						mesPrest = 2;
					} else if (diaVenc > 29 && mesPrest == 1
							&& (int) Math.IEEEremainder(anoPrest, 4.0D) == 0) {
						diaVenc = diaVenc - 29;
						mesPrest = 2;
					} else if (diaVenc > 30) {
						if (mesPrest == 3 || mesPrest == 5 || mesPrest == 6
								|| mesPrest == 8 || mesPrest == 10) {
							diaVenc = diaVenc - 30;
							mesPrest = mesPrest + 1;

						}
					} else if (diaVenc > 31)
						if (mesPrest == 0 || mesPrest == 2 || mesPrest == 4
								|| mesPrest == 7 || mesPrest == 9
								|| mesPrest == 11) {
							diaVenc = diaVenc - 31;
							mesPrest = mesPrest + 1;
							if (mesPrest > 11) {
								mesPrest = 0;
								anoPrest = (1 + anoPrest);
							}
						}
				}

			}

		}
		dbContaParaEditar.close();
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int pAno, int pMes, int pDia) {
			ano = pAno;
			mes = pMes;
			dia = pDia;
			dataConta = (dia + "/" + (1 + mes) + "/" + ano);
			data.setText(dataConta);

		}
	};

	@Override
	protected Dialog onCreateDialog(int paramInt) {
		switch (paramInt) {
		case DIALOGO_DATA_ID:
			return new DatePickerDialog(new ContextThemeWrapper(this,
					R.style.TemaDialogo), mDateSetListener, ano, mes, dia);
		}
		return null;

	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		switch (checkedId) {

		case R.id.rDespContaModificada:
			tipoConta = r.getString(R.string.linha_despesa);
			pagamento.setVisibility(View.VISIBLE);
			break;
		case R.id.rRecContaModificada:
			tipoConta = r.getString(R.string.linha_receita);
			pagamento.setVisibility(View.INVISIBLE);
			break;
		}
		MostraDados();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public void onItemSelected(AdapterView<?> spinner, View arg1, int posicao,
			long arg3) {

		if (spinner.getId() == R.id.spClassificaConta) {
			if (tipoConta.equals(r.getString(R.string.linha_despesa))) {

				dbContaParaEditar.open();
				despesas.moveToPosition(posicao);
				classeConta = despesas.getString(1);
				dbContaParaEditar.close();

			} else {

				dbContaParaEditar.open();
				naoDespesas.moveToPosition(posicao);
				classeConta = naoDespesas.getString(1);
				dbContaParaEditar.close();
			}

		} else {

			switch (posicao) {

			case 0: // Diariamente
				intervalo = 101;
				break;
			case 1: // Semanalmente
				intervalo = 107;
				break;
			case 2: // Mensalmente
				intervalo = 300;
				break;
			case 3: // Anualmente
				intervalo = 3650;
				break;
			}

		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	protected void onDestroy() {
		setResult(RESULT_OK, null);
		super.onDestroy();
	}

}
