package com.msk.minhascontas.paginadores;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.msk.minhascontas.R;
import com.msk.minhascontas.db.DBContas;
import com.msk.minhascontas.info.Ajustes;
import com.msk.minhascontas.listas.ResumoMensalContas;

@SuppressLint("ValidFragment")
public class PaginadorMensalContas extends ActionBarActivity implements
		OnClickListener, OnPageChangeListener {

	private String[] Meses;
	private ImageButton mesAtual;
	private final Calendar c = Calendar.getInstance();
	private int mes, ano, nrPagina;
	private static int[] mesConta, anoConta;

	// BARRA NO TOPO DO APLICATIVO
	private View barraTopo;
	private LayoutInflater inflater;

	// CLASSE DO BANCO DE DADOS
	DBContas dbContas = new DBContas(this);

	// CLASSES PARA PAGINAS
	private Paginas paginasContas;
	private ViewPager mViewPager;

	ActionBar actionBar;

	@SuppressLint("InlinedApi")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.pagina_contabilidade);

		// PEGA O ANO ATUAL PARA DEFINIR A PRIMEIRA TELA
		ano = c.get(1);
		mes = c.get(2);
		ListaMesesAnos();
		Meses = getResources().getStringArray(R.array.MesResumido);

		// PRIMEIRA PAGINA QUE SERA MOSTRADA
		nrPagina = 36;

		// COLOCA O FRAGMENTO NA TELA
		paginasContas = new Paginas(getSupportFragmentManager());
		mViewPager = (ViewPager) findViewById(R.id.paginas);
		usarActionBar();
		mViewPager.setAdapter(paginasContas);
		mViewPager.getAdapter().notifyDataSetChanged();

		// DEFINE O MES QUE APARECERA NA TELA QUANDO ABRIR
		mViewPager.setCurrentItem(nrPagina);

		// RECOMNHECE MUDANCA DE PAGINA
		mViewPager.setOnPageChangeListener(this);

	}

	private void ListaMesesAnos() {
		// DEFINE OS MESES E ANOS QUE APARECERAM NA TELA
		mesConta = new int[96];
		anoConta = new int[96];
		int u = c.get(2);
		int n = c.get(1) - 3;
		for (int i = 0; i < mesConta.length; i++) {

			if (u > 11) {
				u = 0;
				n = n + 1;
			}
			mesConta[i] = u;
			anoConta[i] = n;
			u++;
		}
	}

	/**
	 * CLASSE QUE GERENCIA OS FRAGMENTOS
	 * 
	 * **/
	public static class Paginas extends FragmentStatePagerAdapter {
		public Paginas(FragmentManager fm) {
			super(fm);

		}

		@Override
		public Fragment getItem(int i) {

			// DEFINE MES DA PAGINA NA TELA
			//return ResumoMensalContas.newInstance(mesConta[i], anoConta[i]);
			return ResumoMensalContas.newInstance(mesConta[i], anoConta[i]);
		}

		@Override
		public int getCount() {
			// QUANTIDADE DE PAGINAS QUE SERAO MOSTRADAS
			// DOIS ANOS ANTES E DOIS ANOS DEPOIS DO MES ATUAL
			return 96;
		}

	}

	@Override
	public void onClick(View arg0) {
		// METODOS DOS BOTOES DA BARRA DE RODAPE DA TELA

		mes = mesConta[mViewPager.getCurrentItem()];
		ano = anoConta[mViewPager.getCurrentItem()];

		switch (arg0.getId()) {

		case R.id.botao_mes_atual:
			mViewPager.setCurrentItem(nrPagina);
			break;
		}

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// NAO FAZ NADA

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// MOSTRA ICONE DA PAGINA INICIAL
		if (mViewPager.getCurrentItem() != nrPagina) {
			// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			mesAtual.setVisibility(View.VISIBLE);
		} else {
			// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			mesAtual.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onPageSelected(int arg0) {
		// NAO FAZ NADA

	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versão do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		actionBar = getSupportActionBar();

		inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		barraTopo = inflater.inflate(R.layout.barra_topo, null);

		mesAtual = (ImageButton) barraTopo.findViewById(R.id.botao_mes_atual);

		mesAtual.setOnClickListener(this);
		mesAtual.setVisibility(View.INVISIBLE);
		actionBar.setCustomView(barraTopo);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setDisplayHomeAsUpEnabled(false);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// VERIFICA ANTES SE O APARELHO EH UM CELULAR OU TABLET
		if (this.getResources().getConfiguration().screenLayout >= Configuration.SCREENLAYOUT_SIZE_LARGE
				&& Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
			// Eh um tablet
			getMenuInflater().inflate(R.menu.barra_botoes_tablet, menu);
		} else {
			// Eh um celular
			getMenuInflater().inflate(R.menu.barra_botoes_inicio, menu);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		mes = mesConta[mViewPager.getCurrentItem()];
		ano = anoConta[mViewPager.getCurrentItem()];

		switch (item.getItemId()) {

		case R.id.menu_ajustes:
			startActivityForResult(new Intent(this, Ajustes.class), 0);
			break;
		case R.id.menu_sobre:
			startActivity(new Intent("com.msk.minhascontas.SOBRE"));
			break;
		case R.id.botao_criar:
			startActivityForResult(
					new Intent("com.msk.minhascontas.NOVACONTA"), 1);
			break;

		case R.id.botao_pesquisar:
			startActivityForResult(
					new Intent("com.msk.minhascontas.BUSCACONTA"), 2);
			break;
		case R.id.botao_enviar:

			dbContas.open();
			String aplicacoes = dbContas.mostraContasPorTipo(getResources()
					.getString(R.string.linha_aplicacoes), mes, ano);
			String despesas = dbContas.mostraContasPorTipo(getResources()
					.getString(R.string.linha_despesa), mes, ano);
			String receitas = dbContas.mostraContasPorTipo(getResources()
					.getString(R.string.linha_receita), mes, ano);
			dbContas.close();
			String texto = getResources().getString(R.string.app_name) + " "
					+ Meses[mes] + "/" + ano + "\n" + receitas + "\n"
					+ despesas + "\n" + aplicacoes;

			Intent intento = new Intent("android.intent.action.SEND");
			intento.putExtra("android.intent.extra.SUBJECT",
					getResources().getString(R.string.app_name));
			intento.putExtra("android.intent.extra.TEXT", texto);
			intento.setType("*/*");
			startActivity(Intent.createChooser(intento, getResources()
					.getString(R.string.titulo_grafico)
					+ " "
					+ Meses[mes]
					+ "/" + ano + ":"));

			break;
		case R.id.botao_graficos:

			Bundle dataGrafico = new Bundle();
			dataGrafico.putInt("mes", mes);
			dataGrafico.putInt("ano", ano);
			Intent intentGrafico = new Intent(
					"com.msk.minhascontas.graficos.MEUSGRAFICOS");
			intentGrafico.putExtras(dataGrafico);
			startActivity(intentGrafico);

			break;
		case R.id.botao_listar:

			Bundle dataLista = new Bundle();
			dataLista.putInt("mes", mes);
			dataLista.putInt("ano", ano);
			Intent intentLista = new Intent("com.msk.minhascontas.CONTASDOMES");
			intentLista.putExtras(dataLista);
			startActivityForResult(intentLista, 3);

			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			paginasContas = new Paginas(getSupportFragmentManager());
			mViewPager.setAdapter(paginasContas);
			mViewPager.setCurrentItem(nrPagina);
		}
	}

}
