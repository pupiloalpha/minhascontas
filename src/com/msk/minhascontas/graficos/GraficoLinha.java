package com.msk.minhascontas.graficos;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.graphics.Color;

public class GraficoLinha {

	private int[] x, y;
	private String tipo;
	private int cor = 0;

	public GraphicalView getView(Context context, int[] valoresX, int[] valoresY, String nomeSerie, int corLinha) {

		// Define os dados da s�rie 1
		if (valoresX == null)
			x = new int[0];
		else
			x = valoresX;

		if (valoresY == null)
			y = new int[0];
		else
			y = valoresY; // y values!

		if (nomeSerie == null)
			tipo = "";
		else
			tipo = nomeSerie;
					
		if (corLinha != 0)
			cor = corLinha;
		
		TimeSeries series = new TimeSeries(tipo);

		for (int i = 0; i < x.length; i++) {
			series.add(x[i], y[i]);
		}

		/**
		int[] x2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int[] y2 = { 145, 123, 111, 100, 89, 77, 57, 45, 34, 30 };
		TimeSeries series2 = new TimeSeries("Receitas");
		for (int i = 0; i < x2.length; i++) {
			series2.add(x2[i], y2[i]);
		}*/

		// Adiciona as s�ries no gr�fico
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		dataset.addSeries(series);
		// dataset.addSeries(series2);

		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
		XYSeriesRenderer renderer = new XYSeriesRenderer();
		// XYSeriesRenderer renderer2 = new XYSeriesRenderer();
		mRenderer.addSeriesRenderer(renderer);
		// mRenderer.addSeriesRenderer(renderer2);

		// Modifica cor de fundo do gr�fico
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.WHITE);
		mRenderer.setMarginsColor(Color.WHITE);
		mRenderer.setAxesColor(Color.DKGRAY);

		// Modifica o t�tulo do gr�fico
		mRenderer.setChartTitle("GR�FICO DE LINHA");
		mRenderer.setChartTitleTextSize(20);
		mRenderer.setLabelsColor(Color.BLACK);

		// Define a linha do gr�fico 1
		renderer.setColor(cor);
		renderer.setPointStyle(PointStyle.SQUARE);
		renderer.setLineWidth(3);
		renderer.setFillPoints(true);

		/**
		 * // Define a linha do gr�fico 2 renderer2.setColor(Color.GREEN);
		 * renderer2.setPointStyle(PointStyle.SQUARE);
		 * renderer2.setLineWidth(3); renderer2.setFillPoints(true);
		 */

		// Adiciona os bot�es de Zoom
		mRenderer.setZoomButtonsVisible(true);

		// Intent intent = ChartFactory.getLineChartIntent(context, dataset,
		// mRenderer, "Line Graph Title");
		// return intent;
		return ChartFactory.getLineChartView(context, dataset, mRenderer);
	}

}
