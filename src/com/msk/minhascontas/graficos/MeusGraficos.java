package com.msk.minhascontas.graficos;

import org.achartengine.GraphicalView;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.msk.minhascontas.R;
import com.msk.minhascontas.db.DBContas;

@SuppressWarnings("unused")
public class MeusGraficos extends ActionBarActivity implements OnItemSelectedListener,
		OnClickListener {

	private Spinner tipoGrafico;
	private LinearLayout grafico;
	private ImageButton mesVolta, mesFrente;

	private TextView mesGraficos, semContas;
	private ArrayAdapter<CharSequence> adapter;
	private GraphicalView gView;

	private String[] MESES;
	private int ano, mes, qgraph, contas;

	private double vaplic, vdesp, vfixa, vfun, vpoup, vprest, vprev, vrec,
			vsaldo, vvar, vdesppg, vdespnpg;
	private String tipo, despesa, receita, aplicacao;
	private Cursor despesas, receitas, aplicacoes;

	private float[] valores;
	private String[] series;
	private int[] roleta, cores;

	DBContas dbContasFeitas = new DBContas(this);

	GraficoPizza pie = new GraficoPizza();
	
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.graficos);
		Iniciar();
		usarActionBar();

		Bundle envelope = getIntent().getExtras();
		ano = envelope.getInt("ano");
		mes = envelope.getInt("mes");
		mesGraficos.setText(MESES[mes] + "/" + ano);

		Saldo();
		qgraph = 0;
		tipoGrafico.setOnItemSelectedListener(this);
		mesVolta.setOnClickListener(this);
		mesFrente.setOnClickListener(this);

	}

	private void Iniciar() {
		tipoGrafico = (Spinner) findViewById(R.id.spGrafico);
		grafico = (LinearLayout) findViewById(R.id.mostraGrafico);
		adapter = ArrayAdapter.createFromResource(this, R.array.TipoGrafico,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tipoGrafico.setAdapter(adapter);
		mesGraficos = (TextView) findViewById(R.id.tvMesAno);
		semContas = (TextView) findViewById(R.id.tvSemGrafico);
		mesVolta = (ImageButton) findViewById(R.id.ibMesVolta);
		mesFrente = (ImageButton) findViewById(R.id.ibMesFrente);
		MESES = getResources().getStringArray(R.array.MesesDoAno);

		roleta = new int[] { Color.parseColor("#33B5E5"),
				Color.parseColor("#AA66CC"), Color.parseColor("#99CC00"),
				Color.parseColor("#FFBB33"), Color.parseColor("#FF4444"),
				Color.parseColor("#0099CC"), Color.parseColor("#9933CC"),
				Color.parseColor("#669900"), Color.parseColor("#FF8800"),
				Color.parseColor("#CC0000") };
	}

	private void Saldo() {
		dbContasFeitas.open();

		contas = dbContasFeitas.quantasContasPorMes(mes, ano);

		// DADOS DAS DESPESAS
		despesa = getResources().getString(R.string.linha_despesa);
		despesas = dbContasFeitas.buscaCategoriaPorTipo(despesa);
		vdesp = 0.0D;
		// Valores de despesas
		if (dbContasFeitas.quantasContasPorTipo(despesa, 0, mes, ano) > 0)
			vdesp = dbContasFeitas.somaContas(despesa, 0, mes, ano);

		// DADOS DAS RECEITAS
		receita = getResources().getString(R.string.linha_receita);
		receitas = dbContasFeitas.buscaCategoriaPorTipo(receita);
		vrec = 0.0D;
		// Valores de receitas
		if (dbContasFeitas.quantasContasPorTipo(receita, 0, mes, ano) > 0)
			vrec = dbContasFeitas.somaContas(receita, 0, mes, ano);

		// DADOS DAS APLICACOES
		aplicacao = getResources().getString(R.string.linha_aplicacoes);
		aplicacoes = dbContasFeitas.buscaCategoriaPorTipo(aplicacao);
		vaplic = 0.0D;
		if (dbContasFeitas.quantasContasPorTipo(aplicacao, 0, mes, ano) > 0)
			vaplic = dbContasFeitas.somaContas(aplicacao, 0, mes, ano);

		vsaldo = (vrec - vdesp);

		vdesppg = 0.0D;
		// Valores de despesas pagas
		
		if (dbContasFeitas.quantasContasPagasPorTipo(despesa, "paguei", 0, mes, ano) > 0)
			vdesppg = dbContasFeitas.somaContasPagas(despesa,
					"paguei", 0, mes, ano);
		
		if (dbContasFeitas.quantasContasPagasPorTipo(despesa, "falta", 0, mes, ano) > 0)
			vdespnpg = dbContasFeitas.somaContasPagas(despesa,
					"falta", 0, mes, ano);

		dbContasFeitas.close();

	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {

		switch (arg2) {
		case 0: // Grafico tipo Pizza Comparativo de Contas
			PizzaContas();
			qgraph = 0;
			break;
		case 1: // Grafico tipo Pizza Comparativo de Despesas
			PizzaDespesas();
			qgraph = 1;
			break;
		case 2: // Grafico tipo Pizza Comparativo de Aplicacoes
			PizzaAplicacoes();
			qgraph = 2;
			break;
		case 3:
			PizzaPagamentos();
			qgraph = 3;
		}
		if (contas == 0)
			grafico.setVisibility(View.GONE);
		else
			grafico.setVisibility(View.VISIBLE);
	}

	private void PizzaContas() {
		// Valores e cores do Grafico
		valores = new float[] { (float) vdesp, (float) vrec, (float) vaplic };
		series = getResources().getStringArray(R.array.GraficoContas);
		cores = new int[] { Color.parseColor("#FF4444"),
				Color.parseColor("#33B5E5"), Color.parseColor("#99CC00") };
		// Coloca dados no grafico
		gView = pie.getView(this, valores, cores,
				getString(R.string.dica_grafico_contas), series);
		grafico.removeAllViews();
		grafico.addView(gView);

	}

	private void PizzaAplicacoes() {

		dbContasFeitas.open();
		aplicacoes = dbContasFeitas.buscaCategoriaPorTipo(aplicacao);

		valores = new float[aplicacoes.getCount()];
		series = new String[aplicacoes.getCount()];
		cores = new int[aplicacoes.getCount()];
		roleta = new int[] { Color.parseColor("#33B5E5"),
				Color.parseColor("#AA66CC"), Color.parseColor("#99CC00"),
				Color.parseColor("#FFBB33"), Color.parseColor("#FF4444"),
				Color.parseColor("#0099CC"), Color.parseColor("#9933CC"),
				Color.parseColor("#669900"), Color.parseColor("#FF8800"),
				Color.parseColor("#CC0000") };

		for (int i = 0; i < aplicacoes.getCount(); i++) {
			aplicacoes.moveToPosition(i);
			series[i] = aplicacoes.getString(1);
			if (i > 9) {
				cores[i] = roleta[i - 9];
			} else {
				cores[i] = roleta[i];
			}

			if (dbContasFeitas.quantasContasPorClasse(aplicacoes.getString(1),
					0, mes, ano) > 0)
				valores[i] = (float) dbContasFeitas.somaContasPorClasse(
						aplicacoes.getString(1), 0, mes, ano);
			else
				valores[i] = (float) 0.0D;
		}

		dbContasFeitas.close();

		cores = new int[] { Color.parseColor("#33B5E5"),
				Color.parseColor("#99CC00"), Color.parseColor("#AA66CC") };
		// Coloca dados no grafico
		gView = pie.getView(this, valores, cores,
				getString(R.string.dica_grafico_aplicacoes), series);
		grafico.removeAllViews();
		grafico.addView(gView);

	}

	private void PizzaDespesas() {

		dbContasFeitas.open();
		despesas = dbContasFeitas.buscaCategoriaPorTipo(despesa);

		valores = new float[despesas.getCount()];
		series = new String[despesas.getCount()];
		cores = new int[despesas.getCount()];
		roleta = new int[] { Color.parseColor("#FFBB33"),
				Color.parseColor("#FF4444"), Color.parseColor("#AA66CC"),
				Color.parseColor("#FF8800"), Color.parseColor("#9933CC"),
				Color.parseColor("#CC0000"), Color.parseColor("#0099CC"),
				Color.parseColor("#669900"), Color.parseColor("#33B5E5"),
				Color.parseColor("#99CC00") };

		for (int i = 0; i < despesas.getCount(); i++) {
			despesas.moveToPosition(i);
			series[i] = despesas.getString(1);
			if (i > 9) {
				cores[i] = roleta[i - 9];
			} else {
				cores[i] = roleta[i];
			}
			if (dbContasFeitas.quantasContasPorClasse(despesas.getString(1), 0,
					mes, ano) > 0)
				valores[i] = (float) dbContasFeitas.somaContasPorClasse(
						despesas.getString(1), 0, mes, ano);
			else
				valores[i] = (float) 0.0D;
		}

		dbContasFeitas.close();

		// Coloca dados no grafico
		gView = pie.getView(this, valores, cores,
				getString(R.string.dica_grafico_despesas), series);
		grafico.removeAllViews();
		grafico.addView(gView);

	}

	private void PizzaPagamentos() {
		valores = new float[] { (float) vdespnpg, (float) vdesppg };
		series = getResources().getStringArray(R.array.GraficoPagamentos);
		cores = new int[] { Color.parseColor("#FF4444"),
				Color.parseColor("#FFBB33") };
		// Coloca dados no grafico
		gView = pie.getView(this, valores, cores,
				getString(R.string.dica_grafico_pagamentos), series);
		grafico.removeAllViews();
		grafico.addView(gView);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public void onClick(View graphView) {
		switch (graphView.getId()) {

		case R.id.ibMesVolta:
			mes = (-1 + mes);
			if (mes < 0) {
				mes = 11;
				ano = (-1 + ano);
			}
			mesGraficos.setText(MESES[mes] + "/" + ano);
			Saldo();
			break;
		case R.id.ibMesFrente:
			mes = (1 + mes);
			if (mes > 11) {
				mes = 0;
				ano = (1 + ano);
			}
			mesGraficos.setText(MESES[mes] + "/" + ano);
			Saldo();
			break;
		}
		AtualizaGraficos();

	}

	private void AtualizaGraficos() {

		if (contas == 0)
			grafico.setVisibility(View.GONE);
		else
			grafico.setVisibility(View.VISIBLE);

		// Atualiza o Grafico
		if (qgraph == 0)
			PizzaContas();
		if (qgraph == 1)
			PizzaDespesas();
		if (qgraph == 2)
			PizzaAplicacoes();
		if (qgraph == 3)
			PizzaPagamentos();

	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
