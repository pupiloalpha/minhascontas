package com.msk.minhascontas.graficos;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Context;
import android.graphics.Color;

public class GraficoPizza {

	private String tituloGrafico;
	private String[] serieGrafico;
	private float[] valoresGrafico;
	private int[] coresGrafico;

	public GraphicalView getView(Context context, float[] valores, int[] cores,
			String titulo, String[] serie) {

		// Verifica os dados do gr�fico
		if (valores == null)
			valores = new float[0];
		else
			valoresGrafico = valores;
		if (titulo == null)
			titulo = "";
		else
			tituloGrafico = titulo;
		if (serie == null)
			serie = new String[0];
		else
			serieGrafico = serie;
		if (cores == null)
			cores = new int[0];
		else
			coresGrafico = cores;

		// Coloca dados no gr�fico
		CategorySeries series = new CategorySeries(tituloGrafico);
		int k = 0;
		for (float value : valoresGrafico) {
			series.add(serieGrafico[k], value);
			k++;
		}

		// Define as cores do gr�fico
		DefaultRenderer renderer = new DefaultRenderer();
		for (int cor : coresGrafico) {
			SimpleSeriesRenderer r = new SimpleSeriesRenderer();
			r.setColor(cor);
			renderer.addSeriesRenderer(r);
		}

		// Edita o t�tulo do gr�fico
		renderer.setChartTitle(tituloGrafico);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsColor(Color.BLACK);
		renderer.setLabelsTextSize(15);

		// Modifica cor de fundo do gr�fico
		renderer.setApplyBackgroundColor(true);
		renderer.setBackgroundColor(Color.WHITE);

		// Adiciona os bot�es de zoom
		renderer.setZoomButtonsVisible(true);

		// Intent intent = ChartFactory.getPieChartIntent(context, series,
		// renderer, "Pie");
		return ChartFactory.getPieChartView(context, series, renderer);
	}

}
