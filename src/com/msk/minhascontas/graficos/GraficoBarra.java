package com.msk.minhascontas.graficos;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.graphics.Color;


public class GraficoBarra {

	public GraphicalView getView(Context context) 
	{	
		// Edita os valores da Barra 1
		int[] y = { 10, 20, 30, 40, 50, 35, 25, 15 };
		CategorySeries series = new CategorySeries("Receitas");
		for (int i = 0; i < y.length; i++) {
			series.add("Bar " + (i+1), y[i]);
		}
		
		// Coloca os dados das s�ries no gr�fico
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		dataset.addSeries(series.toXYSeries());


		// This is how the "Graph" itself will look like
		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
		
		// Edita o t�tulo do gr�fico
		mRenderer.setChartTitle("GRAFICO DE BARRA");
		mRenderer.setChartTitleTextSize(20);
		
		// Edita as cores de grade e r�tulos
		mRenderer.setXTitle("VALORES DE X");
		mRenderer.setYTitle("VALORES DE Y");
		mRenderer.setAxesColor(Color.DKGRAY);
	    mRenderer.setLabelsColor(Color.BLACK);
		
	    // Modifica cor de fundo do gr�fico
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.WHITE);
		mRenderer.setMarginsColor(Color.WHITE);
		
	    // Edita a barra 1
		XYSeriesRenderer renderer = new XYSeriesRenderer();
		renderer.setColor(Color.RED);
		renderer.setDisplayChartValues(true);
	    renderer.setChartValuesSpacing((float) 2);
	    mRenderer.addSeriesRenderer(renderer);
 
	    // Adiciona os bot�es de Zoom	    
	    mRenderer.setZoomButtonsVisible(true);
		
	    //Intent intent = ChartFactory.getBarChartIntent(context, dataset, mRenderer, Type.DEFAULT);
		//return intent;
	    return ChartFactory.getBarChartView(context, dataset,mRenderer, Type.DEFAULT);
	}

	
}
