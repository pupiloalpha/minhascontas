package com.msk.minhascontas.listas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.msk.minhascontas.R;
import com.msk.minhascontas.db.DBContas;

@SuppressLint("ValidFragment")
public class ResumoMensal extends Fragment {

	// ELEMENTOS UTILIZADOS EM TELA
	private TextView tabela, categoria, valor;
	private ListView resumo;
	private View linhaView;
	private LayoutInflater inflaLista;

	// VARIAEIS UTILIZADAS
	private int mes, ano, categorias, ajusteReceita;
	private String[] MESES, linhas;
	private double[] valores;
	private String despesa, receita, aplicacao;
	private Cursor despesas, receitas, aplicacoes;
	@SuppressWarnings("rawtypes")
	private ArrayAdapter adapter;

	public static final String ANO_PAGINA = "ano_pagina";
	public static final String MES_PAGINA = "mes_pagina";

	// BARRA NO TOPO DO APLICATIVO
	LayoutInflater inflater;
	Context ctxt;

	// CLASSE DO BANCO DE DADOS
	DBContas dbContas;

	// ELEMENTOS DAS PAGINAS
	private View rootView;
	private static Bundle args;

	public static ResumoMensal newInstance(int mes, int ano) {

		ResumoMensal rm = new ResumoMensal();
		args = new Bundle();
		args.putInt(ANO_PAGINA, ano);
		args.putInt(MES_PAGINA, mes);
		rm.setArguments(args);
		return rm;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		dbContas = new DBContas(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// COLOCA OS MESES NA TELA
		rootView = inflater.inflate(R.layout.contabilidade, container, false);
		args = getArguments();

		mes = args.getInt(MES_PAGINA);
		ano = args.getInt(ANO_PAGINA);

		// COLOCA O MES E O ANO DA CONTA
		MESES = getResources().getStringArray(R.array.MesesDoAno);
		tabela = (TextView) rootView.findViewById(R.id.tvMesLista);
		tabela.setText(MESES[mes] + "/" + ano);

		Saldo();

		resumo = (ListView) rootView.findViewById(R.id.lvResumoContas);

		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, linhas) {

			@Override
			public int getCount() {
				return categorias;
			}

			@Override
			public long getItemId(int idCategoria) {
				return idCategoria;
			}

			@Override
			public View getView(int nrLinha, View lView, ViewGroup parent) {

				linhaView = lView;
				inflaLista = getActivity().getLayoutInflater();
				
				// DEFINE O TIPO DE LINHA QUE SERA CARREGADA
				
				if (nrLinha == 0 || nrLinha == despesas.getCount() + 3
						|| nrLinha == categorias - 3)
					linhaView = inflaLista.inflate(R.layout.linha_tipo, null);
				else if (nrLinha == despesas.getCount() + ajusteReceita + 4)
					linhaView = inflaLista.inflate(R.layout.linha_tipo1, null);
				else if (nrLinha == 1 || nrLinha == 2
						|| nrLinha > categorias - 3)
					linhaView = inflaLista.inflate(R.layout.linha_tipo3, null);
				else
					linhaView = inflaLista.inflate(R.layout.linha_categoria,
							null);

				// NOME E VALOR DE CADA LINHA
				
				categoria = ((TextView) linhaView
						.findViewById(R.id.tvNomeTipoConta));
				valor = ((TextView) linhaView
						.findViewById(R.id.tvValorTipoConta));

				// INSERE OS VALORES EM CADA LINHA
				
				categoria.setText(linhas[nrLinha]);

				valor.setText(getResources().getString(R.string.dica_dinheiro,
						String.format("%.2f", valores[nrLinha])));
				
				// DEFINE AS CORES DAS LINHAS E TEXTOS
				
				if (nrLinha == despesas.getCount() + 3)
					linhaView.setBackgroundResource(R.color.Azul);
				if (nrLinha == categorias - 3)
					linhaView.setBackgroundResource(R.color.Fundo);

				if (ajusteReceita == 0) {
					if (nrLinha > 2 && nrLinha < despesas.getCount() + 3) {
						valor.setTextColor(Color.parseColor("#CC0000"));
					} else if (nrLinha > despesas.getCount() + 4
							&& nrLinha < categorias - 3) {
						valor.setTextColor(Color.parseColor("#669900"));
					}
				} else {
					if (nrLinha > 2 && nrLinha < despesas.getCount() + 3) {
						valor.setTextColor(Color.parseColor("#CC0000"));
					} else if (nrLinha > despesas.getCount() + 3
							&& nrLinha < despesas.getCount() + ajusteReceita
									+ 4) {
						valor.setTextColor(Color.parseColor("#0099CC"));
					} else if (nrLinha > despesas.getCount() + ajusteReceita
							+ 4
							&& nrLinha < categorias - 3) {
						valor.setTextColor(Color.parseColor("#669900"));
					}
				}

				if (valores[categorias - 3] < 0.0D && nrLinha == categorias - 3)
					linhaView.setBackgroundResource(R.color.Vermelho);

				return linhaView;
			}

		};

		resumo.setAdapter(adapter);

		resumo.setOnItemClickListener(listener);

		return rootView;
	}

	private void Saldo() {

		// DEFINE OS NOMES DA LINHAS DA TABELA
		dbContas.open();
		despesa = getResources().getString(R.string.linha_despesa);
		despesas = dbContas.buscaCategoriaPorTipo(despesa);
		receita = getResources().getString(R.string.linha_receita);
		receitas = dbContas.buscaCategoriaPorTipo(receita);
		aplicacao = getResources().getString(R.string.linha_aplicacoes);
		aplicacoes = dbContas.buscaCategoriaPorTipo(aplicacao);

		// AJUSTE QUANDO EXISTE APENAS UMA RECEITA
		if (receitas.getCount() > 1)
			ajusteReceita = receitas.getCount();
		else
			ajusteReceita = 0;

		categorias = despesas.getCount() + ajusteReceita
				+ aplicacoes.getCount() + 8;
		linhas = new String[categorias];
		valores = new double[categorias];

		// PREENCHE AS LINHAS DA TABELA
		// VALORES DE DESPESAS
		linhas[0] = despesa;
		if (dbContas.quantasContasPorTipo(despesa, 0, mes, ano) > 0)
			valores[0] = dbContas.somaContas(despesa, 0, mes, ano);
		else
			valores[0] = 0.0D;
		// VALOR CONTAS PAGAS E A PAGAR
		linhas[1] = getResources()
				.getString(R.string.resumo_pagas);
		if (dbContas.quantasContasPagasPorTipo(despesa, "paguei", 0, mes, ano) > 0)
			valores[1] = dbContas.somaContasPagas(despesa,
					"paguei", 0, mes, ano);
		else
			valores[1] = 0.0D;

		linhas[2] = getResources().getString(
				R.string.resumo_faltam);
		if (dbContas.quantasContasPagasPorTipo(despesa, "falta", 0, mes, ano) > 0)
			valores[2] = dbContas.somaContasPagas(despesa,
					"falta", 0, mes, ano);
		else
			valores[2] = 0.0D;

		for (int i = 0; i < despesas.getCount(); i++) {
			despesas.moveToPosition(i);
			linhas[i + 3] = despesas.getString(1);
			if (dbContas.quantasContasPorClasse(despesas.getString(1), 0, mes,
					ano) > 0)
				valores[i + 3] = dbContas.somaContasPorClasse(
						despesas.getString(1), 0, mes, ano);
			else
				valores[i + 3] = 0.0D;
		}
		// VALORES DE RECEITAS
		linhas[despesas.getCount() + 3] = receita;
		if (dbContas.quantasContasPorTipo(receita, 0, mes, ano) > 0)
			valores[despesas.getCount() + 3] = dbContas.somaContas(receita, 0,
					mes, ano);
		else
			valores[despesas.getCount() + 3] = 0.0D;
		if (receitas.getCount() > 1)
			for (int j = 0; j < receitas.getCount(); j++) {
				receitas.moveToPosition(j);
				linhas[j + despesas.getCount() + 4] = receitas.getString(1);
				if (dbContas.quantasContasPorClasse(receitas.getString(1), 0,
						mes, ano) > 0)
					valores[j + despesas.getCount() + 4] = dbContas
							.somaContasPorClasse(receitas.getString(1), 0, mes,
									ano);
				else
					valores[j + despesas.getCount() + 4] = 0.0D;
			}
		// VALORES DE APLICACOES
		linhas[despesas.getCount() + ajusteReceita + 4] = aplicacao;
		if (dbContas.quantasContasPorTipo(aplicacao, 0, mes, ano) > 0)
			valores[despesas.getCount() + ajusteReceita + 5] = dbContas
					.somaContas(aplicacao, 0, mes, ano);
		else
			valores[despesas.getCount() + ajusteReceita + 5] = 0.0D;
		for (int k = 0; k < aplicacoes.getCount(); k++) {
			aplicacoes.moveToPosition(k);
			linhas[k + despesas.getCount() + ajusteReceita + 5] = aplicacoes
					.getString(1);
			if (dbContas.quantasContasPorClasse(aplicacoes.getString(1), 0,
					mes, ano) > 0)
				valores[k + despesas.getCount() + ajusteReceita + 5] = dbContas
						.somaContasPorClasse(aplicacoes.getString(1), 0, mes,
								ano);
			else
				valores[k + despesas.getCount() + ajusteReceita + 5] = 0.0D;

		}

		// VALOR DO SALDO MENSAL
		linhas[categorias - 3] = getResources().getString(R.string.linha_saldo);

		valores[categorias - 3] = valores[despesas.getCount() + 1] - valores[0];

		// VALOR DO SALDO ATUAL
		linhas[categorias - 2] = getResources()
				.getString(R.string.resumo_saldo);

		valores[categorias - 2] = valores[despesas.getCount() + 1]
				- valores[1];

		// VALOR DO SALDO DO MES ANTERIOR
		linhas[categorias - 1] = getResources()
				.getString(R.string.resumo_receita);

		valores[categorias - 1] = 0.0D;
		
		
		dbContas.close();

	}

	OnItemClickListener listener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// dbContas.open();
			// dbContas.excluiTodasAsCategorias();
			// dbContas.close();
		}

	};

}
