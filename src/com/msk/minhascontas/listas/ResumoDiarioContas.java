package com.msk.minhascontas.listas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.msk.minhascontas.R;
import com.msk.minhascontas.db.DBContas;

@SuppressLint("ValidFragment")
public class ResumoDiarioContas extends Fragment {

	// ELEMENTOS UTILIZADOS EM TELA
	private TextView tabela, categoria, valor;
	private ListView resumo;
	private View linhaView;
	private LayoutInflater inflaLista;

	// VARIAEIS UTILIZADAS
	private int dia, mes, ano, categorias, ajusteReceita;
	private String[] MESES, linhas;
	private double[] valores;
	private String despesa, receita, aplicacao;
	private Cursor despesas, receitas, aplicacoes;
	@SuppressWarnings("rawtypes")
	private ArrayAdapter adapter;

	public static final String ANO_PAGINA = "ano_pagina";
	public static final String MES_PAGINA = "mes_pagina";
	public static final String DIA_PAGINA = "dia_pagina";

	// BARRA NO TOPO DO APLICATIVO
	LayoutInflater inflater;
	Context ctxt;

	// CLASSE DO BANCO DE DADOS
	DBContas dbContas;

	// ELEMENTOS DAS PAGINAS
	private View rootView;
	private static Bundle args;

	public static ResumoDiarioContas newInstance(int dia, int mes, int ano) {

		ResumoDiarioContas rm = new ResumoDiarioContas();
		args = new Bundle();
		args.putInt(ANO_PAGINA, ano);
		args.putInt(MES_PAGINA, mes);
		args.putInt(DIA_PAGINA, dia);
		rm.setArguments(args);
		return rm;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		dbContas = new DBContas(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// COLOCA OS MESES NA TELA
		rootView = inflater.inflate(R.layout.contabilidade, container, false);
		args = getArguments();

		dia = args.getInt(DIA_PAGINA);
		mes = args.getInt(MES_PAGINA);
		ano = args.getInt(ANO_PAGINA);

		// COLOCA O MES E O ANO DA CONTA
		tabela = (TextView) rootView.findViewById(R.id.tvMesLista);
		MESES = getResources().getStringArray(R.array.MesResumido);
		String str = dia + "";
		if (str.length() == 1)
			str = "0" + str;

		tabela.setText(str + "/" + MESES[mes] + "/" + ano);
		
		Saldo();

		resumo = (ListView) rootView.findViewById(R.id.lvResumoContas);

		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, linhas) {

			@Override
			public int getCount() {
				return categorias;
			}

			@Override
			public long getItemId(int idCategoria) {
				return idCategoria;
			}

			@Override
			public View getView(int nrLinha, View lView, ViewGroup parent) {

				linhaView = lView;
				inflaLista = getActivity().getLayoutInflater();
				if (nrLinha == 0 || nrLinha == despesas.getCount() + 1
						|| nrLinha == categorias - 4)
					linhaView = inflaLista.inflate(R.layout.linha_tipo, null);
				else if (nrLinha == despesas.getCount() + ajusteReceita + 2)
					linhaView = inflaLista.inflate(R.layout.linha_tipo1, null);
				else if (nrLinha == categorias - 1)
					linhaView = inflaLista.inflate(R.layout.linha_tipo2, null);
				else
					linhaView = inflaLista.inflate(R.layout.linha_categoria,
							null);

				categoria = ((TextView) linhaView
						.findViewById(R.id.tvNomeTipoConta));
				valor = ((TextView) linhaView
						.findViewById(R.id.tvValorTipoConta));

				categoria.setText(linhas[nrLinha]);

				valor.setText(getResources().getString(R.string.dica_dinheiro,
						String.format("%.2f", valores[nrLinha])));
				if (nrLinha == despesas.getCount() + 1)
					linhaView.setBackgroundResource(R.color.Azul);
				if (nrLinha == categorias - 4)
					linhaView.setBackgroundResource(R.color.Fundo);

				if (ajusteReceita == 0) {
					if (nrLinha > 0 && nrLinha < despesas.getCount() + 1) {
						valor.setTextColor(Color.parseColor("#CC0000"));
					} else if (nrLinha > despesas.getCount() + 2
							&& nrLinha < categorias - 4) {
						valor.setTextColor(Color.parseColor("#669900"));
					} else if (nrLinha == categorias - 3
							|| nrLinha == categorias - 2) {
						categoria.setTextColor(Color.parseColor("#CC0000"));
						valor.setTextColor(Color.parseColor("#CC0000"));
					}
				} else {
					if (nrLinha > 0 && nrLinha < despesas.getCount() + 1) {
						valor.setTextColor(Color.parseColor("#CC0000"));
					} else if (nrLinha > despesas.getCount() + 1
							&& nrLinha < despesas.getCount() + ajusteReceita
									+ 2) {
						valor.setTextColor(Color.parseColor("#0099CC"));
					} else if (nrLinha > despesas.getCount() + ajusteReceita
							+ 2
							&& nrLinha < categorias - 4) {
						valor.setTextColor(Color.parseColor("#669900"));
					} else if (nrLinha == categorias - 3
							|| nrLinha == categorias - 2) {
						categoria.setTextColor(Color.parseColor("#CC0000"));
						valor.setTextColor(Color.parseColor("#CC0000"));
					}
				}

				if (valores[categorias - 4] < 0.0D && nrLinha == categorias - 4)
					linhaView.setBackgroundResource(R.color.Vermelho);

				if (valores[categorias - 1] < 0.0D && nrLinha == categorias - 1)
					valor.setTextColor(Color.parseColor("#CC0000"));

				return linhaView;
			}

		};

		resumo.setAdapter(adapter);

		resumo.setOnItemClickListener(listener);

		return rootView;
	}

	private void Saldo() {

		// DEFINE OS NOMES DA LINHAS DA TABELA
		dbContas.open();
		despesa = getResources().getString(R.string.linha_despesa);
		despesas = dbContas.buscaCategoriaPorTipo(despesa);
		receita = getResources().getString(R.string.linha_receita);
		receitas = dbContas.buscaCategoriaPorTipo(receita);
		aplicacao = getResources().getString(R.string.linha_aplicacoes);
		aplicacoes = dbContas.buscaCategoriaPorTipo(aplicacao);

		// AJUSTE QUANDO EXISTE APENAS UMA RECEITA
		if (receitas.getCount() > 1)
			ajusteReceita = receitas.getCount();
		else
			ajusteReceita = 0;

		categorias = despesas.getCount() + ajusteReceita
				+ aplicacoes.getCount() + 7;
		linhas = new String[categorias];
		valores = new double[categorias];

		// PREENCHE AS LINHAS DA TABELA
		// VALORES DE DESPESAS
		linhas[0] = despesa;
		if (dbContas.quantasContasPorTipo(despesa, dia, mes, ano) > 0)
			valores[0] = dbContas.somaContas(despesa, dia, mes, ano);
		else
			valores[0] = 0.0D;
		for (int i = 0; i < despesas.getCount(); i++) {
			despesas.moveToPosition(i);
			linhas[i + 1] = despesas.getString(1);
			if (dbContas.quantasContasPorClasse(despesas.getString(1), dia,
					mes, ano) > 0)
				valores[i + 1] = dbContas.somaContasPorClasse(
						despesas.getString(1), dia, mes, ano);
			else
				valores[i + 1] = 0.0D;
		}
		// VALORES DE RECEITAS
		linhas[despesas.getCount() + 1] = receita;
		if (dbContas.quantasContasPorTipo(receita, dia, mes, ano) > 0)
			valores[despesas.getCount() + 1] = dbContas.somaContas(receita,
					dia, mes, ano);
		else
			valores[despesas.getCount() + 1] = 0.0D;
		if (receitas.getCount() > 1)
			for (int j = 0; j < receitas.getCount(); j++) {
				receitas.moveToPosition(j);
				linhas[j + despesas.getCount() + 2] = receitas.getString(1);
				if (dbContas.quantasContasPorClasse(receitas.getString(1), dia,
						mes, ano) > 0)
					valores[j + despesas.getCount() + 2] = dbContas
							.somaContasPorClasse(receitas.getString(1), dia,
									mes, ano);
				else
					valores[j + despesas.getCount() + 2] = 0.0D;
			}
		// VALORES DE APLICACOES
		linhas[despesas.getCount() + ajusteReceita + 2] = aplicacao;
		if (dbContas.quantasContasPorTipo(aplicacao, dia, mes, ano) > 0)
			valores[despesas.getCount() + ajusteReceita + 2] = dbContas
					.somaContas(aplicacao, dia, mes, ano);
		else
			valores[despesas.getCount() + ajusteReceita + 2] = 0.0D;
		for (int k = 0; k < aplicacoes.getCount(); k++) {
			aplicacoes.moveToPosition(k);
			linhas[k + despesas.getCount() + ajusteReceita + 3] = aplicacoes
					.getString(1);
			if (dbContas.quantasContasPorClasse(aplicacoes.getString(1), dia,
					mes, ano) > 0)
				valores[k + despesas.getCount() + ajusteReceita + 3] = dbContas
						.somaContasPorClasse(aplicacoes.getString(1), dia, mes,
								ano);
			else
				valores[k + despesas.getCount() + ajusteReceita + 3] = 0.0D;

		}

		// VALOR DO SALDO MENSAL
		linhas[categorias - 4] = getResources().getString(R.string.linha_saldo);

		valores[categorias - 4] = valores[despesas.getCount() + 1] - valores[0];

		// VALOR CONTAS PAGAS E A PAGAR
		linhas[categorias - 3] = getResources()
				.getString(R.string.resumo_pagas);
		if (dbContas
				.quantasContasPagasPorTipo(despesa, "paguei", dia, mes, ano) > 0)
			valores[categorias - 3] = dbContas.somaContasPagas(despesa,
					"paguei", dia, mes, ano);
		else
			valores[categorias - 3] = 0.0D;

		linhas[categorias - 2] = getResources().getString(
				R.string.resumo_faltam);
		if (dbContas.quantasContasPagasPorTipo(despesa, "falta", dia, mes, ano) > 0)
			valores[categorias - 2] = dbContas.somaContasPagas(despesa,
					"falta", dia, mes, ano);
		else
			valores[categorias - 2] = 0.0D;

		// VALOR DO SALDO ATUAL
		linhas[categorias - 1] = getResources()
				.getString(R.string.resumo_saldo);

		valores[categorias - 1] = valores[despesas.getCount() + 1]
				- valores[categorias - 3];

		dbContas.close();

	}

	OnItemClickListener listener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// dbContas.open();
			// dbContas.excluiTodasAsCategorias();
			// dbContas.close();
		}

	};

}
