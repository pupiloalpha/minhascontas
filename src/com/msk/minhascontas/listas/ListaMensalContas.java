package com.msk.minhascontas.listas;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.msk.minhascontas.R;
import com.msk.minhascontas.db.DBContas;
import com.msk.minhascontas.info.Ajustes;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class ListaMensalContas extends ActionBarActivity implements
		View.OnClickListener {

	// ELEMENTOS DA TELA
	private ImageButton antes, depois;
	private ImageView pagamento;
	private LayoutInflater inflaLista;
	private TextView lista, nome, valor, data, semContas;
	private ListView listaContas;
	private View viewLista, lastView;

	// VARIAVEIS UTILIZADAS
	private String[] MESES;
	private int dia, mes, ano;
	private int[] dmaConta;
	private long idConta = 0;
	private String ordemListaDeContas, nomeConta;
	private double valorConta;

	private SimpleCursorAdapter buscaContas;
	private Cursor contasParaLista = null;
	DBContas dbNovasContas = new DBContas(this);

	Calendar c = Calendar.getInstance();

	// BARRA NO TOPO DO APLICATIVO
	ActionMode mActionMode;
	ActionBar actionBar;

	Resources r;

	SharedPreferences buscaPreferencias = null;

	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.contas_do_mes);

		buscaPreferencias = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		ordemListaDeContas = buscaPreferencias.getString("ordem", "conta ASC");
		buscaPreferencias
				.registerOnSharedPreferenceChangeListener(prefListener);

		// Recupera o mes e o ano da lista anterior
		Bundle localBundle = getIntent().getExtras();
		ano = localBundle.getInt("ano");
		mes = localBundle.getInt("mes");
		r = getResources();
		iniciar();
		usarActionBar();

		lista.setText(new StringBuilder().append(MESES[mes]).append("/")
				.append(ano));

		MontaLista();

		// Metodos de click em cada um dos itens da tela
		lastView = null;
		listaContas.setOnItemClickListener(listener);
		antes.setOnClickListener(this);
		depois.setOnClickListener(this);

	}

	OnItemClickListener listener = new OnItemClickListener() {

		@SuppressLint("NewApi")
		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int posicao,
				long arg3) {

			dbNovasContas.open();
			contasParaLista.moveToPosition(posicao);
			idConta = contasParaLista.getLong(0);
			nomeConta = contasParaLista.getString(1);
			dbNovasContas.close();

			if (lastView == null) {
				v.setBackgroundColor(Color.parseColor("#CAFF70"));
				lastView = v;
			} else {
				lastView.setBackgroundColor(Color.WHITE);
				v.setBackgroundColor(Color.parseColor("#CAFF70"));
				lastView = v;
			}

			// if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {

			mActionMode = ListaMensalContas.this
					.startSupportActionMode(new ActionMode.Callback() {

						@Override
						public boolean onCreateActionMode(ActionMode mode,
								Menu menu) {

							MenuInflater inflater = mode.getMenuInflater();
							// VERIFICA ANTES SE O APARELHO EH UM CELULAR OU
							// TABLET
							if (getResources().getConfiguration().screenLayout >= Configuration.SCREENLAYOUT_SIZE_LARGE
									&& Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
								// Eh um tablet
								inflater.inflate(
										R.menu.menu_altera_lista_tablet, menu);
							} else {
								// Eh um celular
								inflater.inflate(R.menu.menu_altera_lista, menu);
							}

							return true;
						}

						@Override
						public boolean onPrepareActionMode(ActionMode mode,
								Menu menu) {

							mode.setTitle(nomeConta);

							return true;
						}

						@Override
						public boolean onActionItemClicked(ActionMode mode,
								MenuItem item) {
							switch (item.getItemId()) {
							case R.id.botao_editar:
								if (idConta != 0) {
									Bundle localBundle = new Bundle();
									localBundle.putLong("id", idConta);
									Intent localIntent = new Intent(
											"com.msk.minhascontas.EDITACONTA");
									localIntent.putExtras(localBundle);
									startActivityForResult(localIntent, 1);
								}
								buscaContas.notifyDataSetChanged();
								MontaLista();
								mode.finish();
								break;
							case R.id.botao_pagar:
								if (idConta != 0) {
									dbNovasContas.open();
									String pg = dbNovasContas
											.mostraPagamentoConta(idConta);
									if (pg.equals("paguei")) {
										dbNovasContas.alteraPagamentoConta(
												idConta, "falta");
									} else {
										dbNovasContas.alteraPagamentoConta(
												idConta, "paguei");
									}
									dbNovasContas.close();
								}
								buscaContas.notifyDataSetChanged();
								MontaLista();
								mode.finish();
								break;
							case R.id.botao_excluir:
								if (idConta != 0) {
									dbNovasContas.open();
									String nomeContaExcluir = dbNovasContas
											.mostraNomeConta(idConta);
									int qtRepeteConta = dbNovasContas
											.quantasContasPorNome(nomeContaExcluir);
									if (qtRepeteConta == 1) {
										dbNovasContas.excluiConta(idConta);
										Toast.makeText(
												getApplicationContext(),
												getResources()
														.getString(
																R.string.dica_conta_excluida,
																nomeContaExcluir),
												Toast.LENGTH_SHORT).show();
										idConta = 0;
									} else {
										Dialogo();
									}
									dbNovasContas.close();
								}
								buscaContas.notifyDataSetChanged();
								MontaLista();
								mode.finish();
								break;
							case R.id.botao_lembrete:
								if (idConta != 0) {
									dbNovasContas.open();
									dmaConta = dbNovasContas
											.mostraDMAConta(idConta);
									dia = dmaConta[0];
									mes = dmaConta[1];
									ano = dmaConta[2];

									valorConta = dbNovasContas
											.mostraValorConta(idConta);
									String nomeContaCalendario = r.getString(
											R.string.dica_evento, dbNovasContas
													.mostraNomeConta(idConta));
									dbNovasContas.close();
									c.set(ano, mes, dia);
									Intent evento = new Intent(
											Intent.ACTION_EDIT);
									evento.setType("vnd.android.cursor.item/event");
									evento.putExtra(Events.TITLE,
											nomeContaCalendario);
									evento.putExtra(Events.DESCRIPTION, r
											.getString(
													R.string.dica_calendario,
													String.format("%.2f",
															valorConta)));

									evento.putExtra(
											CalendarContract.EXTRA_EVENT_BEGIN_TIME,
											c.getTimeInMillis());
									evento.putExtra(
											CalendarContract.EXTRA_EVENT_END_TIME,
											c.getTimeInMillis());

									evento.putExtra(Events.ACCESS_LEVEL,
											Events.ACCESS_PRIVATE);
									evento.putExtra(Events.AVAILABILITY,
											Events.AVAILABILITY_BUSY);
									startActivity(evento);
								}
								buscaContas.notifyDataSetChanged();
								MontaLista();
								mode.finish();
								break;
							}
							return true;

						}

						@Override
						public void onDestroyActionMode(ActionMode mode) {
							mActionMode = null;
						}

					});

		}

	};

	private void iniciar() {
		listaContas = ((ListView) findViewById(R.id.lvContasCriadas));
		antes = ((ImageButton) findViewById(R.id.ibMesAntes));
		depois = ((ImageButton) findViewById(R.id.ibMesDepois));
		lista = ((TextView) findViewById(R.id.tvMesLista));
		semContas = (TextView) findViewById(R.id.tvSemContas);
		MESES = getResources().getStringArray(R.array.MesesDoAno);
	}

	@SuppressWarnings("deprecation")
	private void MontaLista() {
		dbNovasContas.open();
		contasParaLista = dbNovasContas.buscaTodasDoMes(mes, ano,
				buscaPreferencias.getString("ordem", ordemListaDeContas));
		int n = contasParaLista.getCount();
		dbNovasContas.close();
		if (n >= 0) {
			buscaContas = new SimpleCursorAdapter(this, R.id.lvContasCriadas,
					contasParaLista, new String[] { "conta", "data", "valor" },
					new int[] { R.id.tvNomeContaCriada, R.id.tvDataContaCriada,
							R.id.tvValorContaCriada }) {

				public View getView(int nrLinha, View linhaView,
						ViewGroup pViewGroup) {
					viewLista = linhaView;
					inflaLista = getLayoutInflater();
					viewLista = inflaLista.inflate(R.layout.linha_conta, null);
					nome = ((TextView) viewLista
							.findViewById(R.id.tvNomeContaCriada));
					data = ((TextView) viewLista
							.findViewById(R.id.tvDataContaCriada));
					valor = ((TextView) viewLista
							.findViewById(R.id.tvValorContaCriada));
					pagamento = ((ImageView) viewLista
							.findViewById(R.id.ivPagamento));
					dbNovasContas.open();

					contasParaLista.moveToPosition(nrLinha);
					int i = contasParaLista.getInt(10);
					String status = contasParaLista.getString(4);
					String classe = contasParaLista.getString(3);
					String tipo = contasParaLista.getString(2);
					String nomeconta = contasParaLista.getString(1);

					dbNovasContas.close();

					String[] prestacao = r.getStringArray(R.array.TipoDespesa);

					if ((i != 0) && (classe.equals(prestacao[2]))) {
						String str4 = nomeconta + " "
								+ contasParaLista.getInt(11) + "/" + i;
						nome.setText(str4);
					} else {
						nome.setText(contasParaLista.getString(1));
					}

					data.setText(contasParaLista.getString(5));

					valor.setText(getResources()
							.getString(
									R.string.dica_dinheiro,
									String.format("%.2f",
											contasParaLista.getDouble(9))));

					if (tipo.equals(r.getString(R.string.linha_despesa))) {
						valor.setTextColor(Color.parseColor("#CC0000"));
					} else if (tipo.equals(r
							.getString(R.string.linha_aplicacoes))) {
						valor.setTextColor(Color.parseColor("#669900"));
					} else {
						valor.setTextColor(Color.parseColor("#0099CC"));
					}

					if (status.equals("paguei")) {
						pagamento.setVisibility(View.VISIBLE);
					} else {
						pagamento.setVisibility(View.INVISIBLE);
					}

					return viewLista;
				}

				public int getCount() {
					dbNovasContas.open();
					int i = dbNovasContas.quantasContasPorMes(mes, ano);
					dbNovasContas.close();
					return i;
				}

				public long getItemId(int idConta) {
					return idConta;
				}

			};

			listaContas.setAdapter(buscaContas);
			listaContas.setEmptyView(semContas);

		}

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.ibMesAntes:
			mes = (-1 + this.mes);
			if (mes < 0) {
				mes = 11;
				ano = (-1 + this.ano);
			}
			break;

		case R.id.ibMesDepois:
			mes = (1 + this.mes);
			if (mes > 11) {
				mes = 0;
				ano = (1 + this.ano);
			}
			break;
		}
		lista.setText(new StringBuilder().append(MESES[mes]).append("/")
				.append(ano));
		buscaContas.notifyDataSetChanged();
		MontaLista();

		setResult(RESULT_OK, null);
	}

	private void Dialogo() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				new ContextThemeWrapper(this, R.style.TemaDialogo));

		// set title
		alertDialogBuilder.setTitle(getString(R.string.dica_menu_exclusao));

		// set dialog message
		alertDialogBuilder.setItems(R.array.TipoAjusteConta,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dbNovasContas.open();
						String nomeContaExcluir = dbNovasContas
								.mostraNomeConta(idConta);
						switch (id) {
						case 0:
							dbNovasContas.excluiConta(idConta);
							break;
						case 1:
							int[] repete = dbNovasContas
									.mostraRepeticaoConta(idConta);
							int nr = repete[1];
							dbNovasContas.excluiSerieContaPorNome(
									nomeContaExcluir, nr);
							break;
						case 2:
							dbNovasContas.excluiContaPorNome(nomeContaExcluir);
							break;
						}
						buscaContas.notifyDataSetChanged();
						dbNovasContas.close();
						MontaLista();
						idConta = 0;
						nomeConta = " ";
						setResult(RESULT_OK, null);
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			MontaLista();
		}
	}

	protected void onRestart() {
		super.onRestart();
		MontaLista();
	}

	protected void onResume() {
		super.onResume();
		MontaLista();
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
			listaContas.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		getMenuInflater().inflate(R.menu.barra_botoes_lista, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		case R.id.menu_ajustes:
			startActivityForResult(new Intent(this, Ajustes.class), 0);
			break;
		case R.id.menu_sobre:
			startActivity(new Intent("com.msk.minhascontas.SOBRE"));
			break;
		case R.id.botao_criar:
			startActivityForResult(
					new Intent("com.msk.minhascontas.NOVACONTA"), 1);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private OnSharedPreferenceChangeListener prefListener = new OnSharedPreferenceChangeListener() {

		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			if (key.equals("ordem")) {
				MontaLista();
			}

		}
	};

	@Override
	protected void onDestroy() {
		setResult(RESULT_OK, null);
		super.onDestroy();
	}
}