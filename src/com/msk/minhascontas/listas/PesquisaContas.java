package com.msk.minhascontas.listas;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.msk.minhascontas.R;
import com.msk.minhascontas.db.DBContas;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class PesquisaContas extends ActionBarActivity implements View.OnClickListener {

	// ELEMENTOS DA TELA
	private ImageButton buscaConta;
	private ImageView pagamento;
	private LayoutInflater inflaLista;
	private ListView listaContas;
	private View viewLista, lastView;
	private TextView data, nome, valor, resultado;
	private AutoCompleteTextView nomeContaBuscar;

	// ELEMENTOS PARA MONTAR A LISTA
	private SimpleCursorAdapter buscaContas;
	private Cursor contasParaLista = null;
	@SuppressWarnings("rawtypes")
	private ArrayAdapter completa;

	DBContas dbContasPesquisadas = new DBContas(this);
	Calendar c = Calendar.getInstance();
	Resources r;

	// VARIAVEIS
	private long idConta = 0;
	private String nomeBuscado;
	private int dia, mes, ano;
	private int[] dmaConta;
	private double valorConta;

	ActionBar actionBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.pesquisa_conta);
		setTheme(android.R.style.Theme);
		r = getResources();
		iniciar();
		usarActionBar();
		MontaAutoCompleta();
		buscaConta.setOnClickListener(this);
		lastView = null;
		listaContas.setOnItemClickListener(listener);

	}

	private void iniciar() {

		listaContas = ((ListView) findViewById(R.id.lvContasPesquisadas));
		buscaConta = ((ImageButton) findViewById(R.id.ibBuscaConta));
		resultado = ((TextView) findViewById(R.id.tvSemResultados));

		listaContas.setEmptyView(resultado);

		nomeContaBuscar = ((AutoCompleteTextView) findViewById(R.id.acNomeContaBusca));

	}

	OnItemClickListener listener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adaptView, View v, int posicao,
				long arg3) {

			dbContasPesquisadas.open();
			contasParaLista.moveToPosition(posicao);
			idConta = contasParaLista.getLong(0);
			dbContasPesquisadas.close();

			if (lastView == null) {
				v.setBackgroundColor(Color.parseColor("#CAFF70"));
				lastView = v;
			} else {
				lastView.setBackgroundColor(Color.WHITE);
				v.setBackgroundColor(Color.parseColor("#CAFF70"));
				lastView = v;
			}
		}
	};

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void MontaAutoCompleta() {
		dbContasPesquisadas.open();
		if (dbContasPesquisadas.quantasContas() != 0) {

			completa = new ArrayAdapter(this,
					android.R.layout.simple_dropdown_item_1line,
					dbContasPesquisadas.mostraNomeContas());
		} else {
			completa = new ArrayAdapter(this,
					android.R.layout.simple_dropdown_item_1line, getResources()
							.getStringArray(R.array.NomeConta));
		}

		dbContasPesquisadas.close();
		nomeContaBuscar.setAdapter(completa);
	}

	@SuppressWarnings("deprecation")
	private void MontaLista() {
		dbContasPesquisadas.open();
		contasParaLista = dbContasPesquisadas.buscaContasPorNome(nomeBuscado);
		int i = contasParaLista.getCount();
		dbContasPesquisadas.close();
		if (i >= 0) {
			buscaContas = new SimpleCursorAdapter(this, R.id.lvContasCriadas,
					contasParaLista, new String[] { "conta", "data", "valor" },
					new int[] { R.id.tvNomeContaCriada, R.id.tvDataContaCriada,
							R.id.tvValorContaCriada }) {

				public View getView(int nrLinha, View linhaView,
						ViewGroup pViewGroup) {
					viewLista = linhaView;
					inflaLista = PesquisaContas.this.getLayoutInflater();
					viewLista = inflaLista.inflate(R.layout.linha_conta, null);
					nome = ((TextView) viewLista
							.findViewById(R.id.tvNomeContaCriada));
					data = ((TextView) viewLista
							.findViewById(R.id.tvDataContaCriada));
					valor = ((TextView) viewLista
							.findViewById(R.id.tvValorContaCriada));
					pagamento = ((ImageView) viewLista
							.findViewById(R.id.ivPagamento));
					dbContasPesquisadas.open();

					contasParaLista.moveToPosition(nrLinha);
					int i = contasParaLista.getInt(10);
					String status = contasParaLista.getString(4);
					String classe = contasParaLista.getString(3);
					String tipo = contasParaLista.getString(2);
					String nomeconta = contasParaLista.getString(1);

					dbContasPesquisadas.close();

					String[] prestacao = r.getStringArray(R.array.TipoDespesa);

					if ((i != 0) && (classe.equals(prestacao[2]))) {
						String str4 = nomeconta + " "
								+ contasParaLista.getInt(11) + "/" + i;
						nome.setText(str4);
					} else {
						nome.setText(contasParaLista.getString(1));
					}

					data.setText(contasParaLista.getString(5));

					valor.setText(getResources()
							.getString(
									R.string.dica_dinheiro,
									String.format("%.2f",
											contasParaLista.getDouble(9))));

					if (tipo.equals(r.getString(R.string.linha_despesa))) {
						valor.setTextColor(Color.parseColor("#CC0000"));
					} else if (tipo.equals(r
							.getString(R.string.linha_aplicacoes))) {
						valor.setTextColor(Color.parseColor("#669900"));
					} else {
						valor.setTextColor(Color.parseColor("#0099CC"));
					}

					if (status.equals("paguei")) {
						pagamento.setVisibility(View.VISIBLE);
					} else {
						pagamento.setVisibility(View.INVISIBLE);
					}

					return viewLista;

				}

				public int getCount() {
					dbContasPesquisadas.open();
					int i = dbContasPesquisadas
							.quantasContasPorNome(nomeBuscado);
					dbContasPesquisadas.close();
					return i;
				}

				public long getItemId(int idConta) {
					return idConta;
				}

			};

			listaContas.setAdapter(buscaContas);
			listaContas.setEmptyView(resultado);
		}

	}

	@Override
	public void onClick(View arg0) {

		switch (arg0.getId()) {

		case R.id.ibBuscaConta:

			if (nomeContaBuscar.getText().toString().equals(""))
				nomeBuscado = " ";
			else {
				nomeBuscado = nomeContaBuscar.getText().toString();
			}

			MontaLista();
			MontaAutoCompleta();
			nomeContaBuscar.setText("");
			idConta = 0;
			break;
		}
	}

	private void Dialogo() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				new ContextThemeWrapper(this, R.style.TemaDialogo));

		alertDialogBuilder.setTitle(getString(R.string.dica_menu_exclusao));

		alertDialogBuilder.setItems(R.array.TipoAjusteConta,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dbContasPesquisadas.open();
						String nomeContaExcluir = dbContasPesquisadas
								.mostraNomeConta(idConta);
						switch (id) {
						case 0:
							dbContasPesquisadas.excluiConta(idConta);
							break;
						case 1:
							int[] repete = dbContasPesquisadas
									.mostraRepeticaoConta(idConta);
							int nr = repete[1];
							dbContasPesquisadas.excluiSerieContaPorNome(
									nomeContaExcluir, nr);
							break;
						case 2:
							dbContasPesquisadas
									.excluiContaPorNome(nomeContaExcluir);
							break;
						}
						buscaContas.notifyDataSetChanged();
						MontaLista();
						MontaAutoCompleta();
						dbContasPesquisadas.close();
						idConta = 0;
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
				listaContas.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.menu_altera_lista, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		case R.id.botao_editar:

			if (idConta != 0) {
				Bundle nova = new Bundle();
				nova.putLong("id", idConta);
				Intent app = new Intent(
						"com.msk.minhascontas.EDITACONTA");
				app.putExtras(nova);
				PesquisaContas.this.startActivityForResult(app, 1);
			}
			setResult(RESULT_OK, null);
			break;
		case R.id.botao_excluir:

			if (idConta != 0) {
				dbContasPesquisadas.open();
				String nomeContaExcluir = dbContasPesquisadas
						.mostraNomeConta(idConta);
				int qtRepeteConta = dbContasPesquisadas
						.quantasContasPorNome(nomeContaExcluir);
				if (qtRepeteConta == 1) {
					dbContasPesquisadas.excluiConta(idConta);
					Toast.makeText(
							PesquisaContas.this.getApplicationContext(),
							getResources().getString(
									R.string.dica_conta_excluida,
									nomeContaExcluir), Toast.LENGTH_SHORT)
							.show();
					idConta = 0;
				} else {
					Dialogo();
				}
				dbContasPesquisadas.close();
				buscaContas.notifyDataSetChanged();
				MontaLista();
				setResult(RESULT_OK, null);
			}
			break;
		case R.id.botao_pagar:
			if (idConta != 0) {
				dbContasPesquisadas.open();
				String pg = dbContasPesquisadas.mostraPagamentoConta(idConta);
				if (pg.equals("paguei")) {
					dbContasPesquisadas.alteraPagamentoConta(idConta, "falta");
				} else {
					dbContasPesquisadas.alteraPagamentoConta(idConta, "paguei");
				}
				dbContasPesquisadas.close();
				idConta = 0;
				MontaLista();
				setResult(RESULT_OK, null);
			}
			break;
		case R.id.botao_lembrete:
			if (idConta != 0) {
				dbContasPesquisadas.open();
				dmaConta = dbContasPesquisadas.mostraDMAConta(idConta);
				dia = dmaConta[0];
				mes = dmaConta[1];
				ano = dmaConta[2];

				valorConta = dbContasPesquisadas.mostraValorConta(idConta);
				String nomeContaCalendario = r.getString(R.string.dica_evento,
						dbContasPesquisadas.mostraNomeConta(idConta));
				dbContasPesquisadas.close();
				c.set(ano, mes, dia);
				Intent evento = new Intent(Intent.ACTION_EDIT);
				evento.setType("vnd.android.cursor.item/event");
				evento.putExtra(Events.TITLE, nomeContaCalendario);
				evento.putExtra(
						Events.DESCRIPTION,
						r.getString(R.string.dica_calendario,
								String.format("%.2f", valorConta)));

				evento.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
						c.getTimeInMillis());
				evento.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
						c.getTimeInMillis());

				evento.putExtra(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
				evento.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
				setResult(RESULT_OK, null);
				startActivity(evento);
			}
			break;
		}

		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			buscaContas.notifyDataSetChanged();
			MontaLista();
			idConta = 0;
		}
	}

	@Override
	protected void onDestroy() {
		setResult(RESULT_OK, null);
		super.onDestroy();
	}
	
}
