package com.msk.minhascontas.info;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.widget.TextView;

import com.msk.minhascontas.R;

public class Sobre extends Activity {

	private TextView sobre;
	private  PackageInfo pinfo;
	private String versao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_sobre);
		
		 sobre = (TextView) findViewById(R.id.tvSobre);
		
		 try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		 versao = pinfo.versionName;
		 
		 sobre.setText(getResources().getString(R.string.sobre, versao));
		 
	}

	
}
