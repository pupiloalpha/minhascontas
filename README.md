MinhasContas
============

Aplicativo para plataforma Android que ajuda no controle de gastos mensais

Notas das versões desenvolvidas:

1.0 - Cria conta, edita e salva no banco de dados;
1.1 - Mostra resumo mensal das contas;
1.2 - Mostra gráfico de comparação das contas;
1.3 - Envia resumo das contas por mensagem ou e-mail;
1.4 - Projeto controlado no github;
1.4.1 - Opções detalhadas de edição e exclusão de contas com repetição (estilo agenda do Google);

2.0 - Novo visual do aplicativo;
2.1 - Nova plataforma de gráficos de comparação (Biblioteca AChartEngine);
2.2 - Gráficos de comparação do tipo pizza;
2.3 - Opção de pesquisa de contas por nome;
2.3.1 - Acréscimo de listagem vazia como padrão;
2.3.2 - Disponibilidade para AppMarcket;
2.3.3 - Suporte para telas menores;
2.4 - Utilização de Fragments e ViewPager;
2.4.1 - Página para lista mensal das contas;
2.4.2 - Página para resumo mensal;
2.4.3 - Página para graficos;
2.5 - Suporte para inglês e espanhol;
2.6 - Exportar para excel;
